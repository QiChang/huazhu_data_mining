# !/usr/bin/env python
# -*- coding: utf-8 -*-
import tornado.web
import os
import json
import shutil

cluster_file = os.path.join(os.path.dirname(__file__), '../static/cluster_txt')


class FileUploadHandler(tornado.web.RequestHandler):

    REDIS_EXPIRES = 1 * 24 * 60 * 60
    def options(self, *args, **kwargs):
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        self.finish({"OK" : 0})

    def get(self, *args, **kwargs):
        self.finish('''
        <html>
          <head><title>Upload File</title></head>
          <body>
            <form action='upload' enctype="multipart/form-data" method='post'>
            <input type='file' name='file'/><br/>
            <input type='submit' value='submit'/>
            </form>
          </body>
        </html>
        ''')

    def post(self, *args, **kwargs):
        ret = {'result': 'OK'}
        isNotCvs = 1
        upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
        file_metas = self.request.files.get('file', None)  # 提取表单中‘name’为‘file’的文件元数据
        if not file_metas:
            ret['result'] = 'Invalid Args'
            return ret

        for meta in file_metas:
            if meta['filename'].split(".")[1] in ("xlsx","xls"):
                filename = "input.xlsx"
                if os.path.isfile(os.path.join(upload_path, "input.csv")):
                    try:
                        os.remove(os.path.join(upload_path, "input.csv"))
                        print("删除文件 {} 成功".format(os.path.join(upload_path, "input.csv")))
                    except Exception as e:
                        print("删除文件 {} 失败".format(os.path.join(upload_path, "input.csv")))
                        print(e)
            else:
                filename = "input.csv"
                if os.path.isfile(os.path.join(upload_path,"input.xlsx")):
                    try:
                        os.remove(os.path.join(upload_path, "input.xlsx"))
                        print("删除文件 {} 成功".format(os.path.join(upload_path, "input.xlsx")))
                    except Exception as e:
                        print("删除文件 {} 失败".format(os.path.join(upload_path, "input.xlsx")))
                        print(e)
                if os.path.isfile(os.path.join(upload_path,"input.xls")):
                    try:
                        os.remove(os.path.join(upload_path, "input.xls"))
                        print("删除文件 {} 成功".format(os.path.join(upload_path, "input.xls")))
                    except Exception as e:
                        print("删除文件 {} 失败".format(os.path.join(upload_path, "input.xls")))
                        print(e)
                # isNotCvs = 0
            print(filename)
            file_path = os.path.join(upload_path, filename)
            print("FileUploadHandler : {} {}".format(file_path,filename))
            with open(file_path, 'wb') as up:
                up.write(meta['body'])

        print("==============================")
        # redis_cli = utils.get_redis_cli()
        # redis_cli.set("cluster_status", "prepare", ex=self.REDIS_EXPIRES)
        # redis_cli.set("file_type", isNotCvs, ex=self.REDIS_EXPIRES)
        # print("FileUploadHandler {}".format(redis_cli.get("cluster_status")))

        self.set_header('Access-Control-Allow-Origin', '*')
        self.write(json.dumps(ret))


# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/upload/*.*', FileUploadHandler)
]

# Sept-12-2018  QiChang.Yin         create