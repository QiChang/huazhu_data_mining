# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
from common import  cluster
import pandas as pd
import jieba
import jieba.analyse
import utils
import sys
from gsdmm import aaa

from celery_app.callback import callback
from pandas.core.frame import DataFrame
import datetime
import os
class CountClusterHandler(tornado.web.RequestHandler):

    REDIS_EXPIRES = 1 * 24 * 60 * 60

    def options(self):
        # self.set_header('Access-Control-Allow-Credentials:true')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        # self.set_status(204)
        self.finish()

    # def get(self):
    #     # file = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    #     # read txt method three
    #     # f2 = open("./image/abc.txt", "r")
    #     # lines = f2.readlines()
    #     # for line3 in lines:
    #     #      print
    #     #     line3
    #     file_name = "1.txt"
    #     # self.set_header('Content-Type', 'application/octet-stream')
    #     # self.set_header('Access-Control-Allow-Origin', '*')
    #     # self.set_header('Access-Control-Allow-Origin:'.$_SERVER["HTTP_ORIGIN"]
    #     # self.set_header('Access-Control-Allow-Credentials:true')
    #     self.set_header('Access-Control-Allow-Headers', '*')
    #     self.set_header("Access-Control-Allow-Origin", "*")
    #     self.set_header('Access-Control-Allow-Methods', '*')
    #     # self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
    #     file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/cluster/")
    #     path = os.path.join(file_dir, file_name)
    #     print(path)
    #     with open(path) as f:
    #         self.set_header('Access-Control-Allow-Origin', '*')
    #         # self.write(str(f.readlines()).strip())
    #         # print(type(f.readlines()).__name__)
    #         # print(list(f.readlines()))
    #
    #         nice = f.readlines()[0].replace("[","").replace("]","").split('&&')
    #         for  i  in range(len(nice)):
    #             # print(type(nice(i)))
    #             nice[i] = tuple(eval(nice[i]))
    #         self.write({"data":nice})
    #     self.finish()
    #         # for line in f.readlines():
    #         #     print(line)
    #     f.close()


    @tornado.gen.coroutine
    def post(self, *args, **kwargs):

        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        try:
            json_params = json.loads(str(self.request.body, encoding='utf-8'))
            params = json_params.get('params','0')
            end = params.get('end','0')
            start = params.get('start', '0')
            type = params.get('type', '0')
            # upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
            # os.remove(upload_path + "/output.xls")
            print(start)
            print(end)
            file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
            print(file_dir)
            redis_cli = utils.get_redis_cli()
            print(redis_cli.get("cluster_status"))
            print(redis_cli.get("file_type"))
            # if redis_cli.get("cluster_status") == "prepare":
            #     pid = os.fork()
            #     upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
            #     if pid < 0:
            #         print("create process failed")
            #     elif pid == 0:
                    # print("this is child process")
                    # redis_cli.set("cluster_status", "begin", ex=self.REDIS_EXPIRES)
                    # cluster_dir = os.path.join(os.path.dirname(__file__), '../static/cluster_txt')
                    # IDF = pd.read_table(cluster_dir + "/chinese-idf-wordlist.txt", names=['word', 'number'],
                    #                     sep='\t').set_index('word').T.to_dict('records')[0]
                    #
                    # SYNONYM_LIST = pd.read_table(cluster_dir + "/syn_word_dup.txt",
                    #                              names=['word', 'word_syn'], error_bad_lines=False).set_index('word').T.to_dict(
                    #     'records')[0]
                    # print("asdsaasd")
                    # # dir_name = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
                    # # cluster_dataset = os.path.join(os.path.dirname(__file__), '../static/upload')  # 文件的暂存路径
                    # cluster_dataset_path = upload_path + "/input.xlsx" if redis_cli.get("file_type") else upload_path + "/input.csv"
                    # # cluster_dataset_path = upload_path + "/input.xls" if isNotCvs else upload_path+"/input.csv"
                    # print(upload_path,cluster_dataset_path)
                    # try:
                    #     if type == str(0):
                    #         # dic = cluster.question_cluster(cluster_dataset_path, IDF, SYNONYM_LIST)
                    #         # dic.to_excel(upload_path + '/output.xls', sheet_name='output', index=False)
                    #
                    #         starttime = datetime.datetime.now()
                    #         length_sent, length_set, length_cluster, h3, length_ceshi_count, dic, repre = cluster.question_cluster(
                    #             cluster_dataset_path, IDF, SYNONYM_LIST)
                    #
                    #         data = DataFrame(repre)
                    #         dic.to_excel(upload_path + "/data1222.xls", index=False)
                    #         data.to_excel(upload_path + "/reper111.xls", index=False)
                    #         endtime = datetime.datetime.now()
                    #         length_time = (endtime - starttime).seconds
                    #         count_dic = {}
                    #         count_dic['最小类元素的个数'] = 5
                    #         count_dic['聚类时间'] = length_time
                    #         count_dic['总共数据量'] = length_sent
                    #         count_dic['去重后条数'] = length_set
                    #         count_dic['共聚出条数'] = length_cluster
                    #         count_dic['有意义的类数'] = length_cluster - 1
                    #         count_dic['有意义的类中问题频率和'] = h3
                    #         count_dic['有意义的类中问题去重后'] = length_ceshi_count
                    #         count_dic['无意义类数'] = 1
                    #         count_df = pd.DataFrame([count_dic])
                    #         count_df = count_df.T
                    #         count_df.to_excel(upload_path + "/count.xls")
                    #     else:
                    #         aaa.get_result(cluster_dataset_path, upload_path + '/output.xls')
                    # except Exception as e:
                    #     print(e)
                    # redis_cli.set("cluster_status", "finish", ex=self.REDIS_EXPIRES)

            #     else:
            #         print("this is parent process")
            #         self.set_header('Access-Control-Allow-Origin', '*')
            #         self.write({"data": [], "all_num": 0})
            # else:
            # if redis_cli.get("cluster_status") == "finish":
            #     path = os.path.join(file_dir, "count.xls")
            #     if os.path.isfile(path):
            #         print(path)
            #         dd = pd.read_excel(path)
            #         nice = [[i, j] for i, j in zip(dd.iloc[:,0], dd.iloc[:,1])]
            #         all_num = len(nice)
            #         nice = nice[int(start):int(end) + 1]
            #         self.set_header('Access-Control-Allow-Origin', '*')
            #         self.write({"data": nice, "all_num": all_num})
            # else:
            #     self.set_header('Access-Control-Allow-Origin', '*')
            #     self.write({"data": [], "all_num": 0})

                # self.finish()
        except Exception as e:
            print(e)



# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/count_cluster/*.*', CountClusterHandler)
]

# Sept-12-2018  QiChang.Yin         create