# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
from common import  cluster
import pandas as pd
import jieba
import jieba.analyse
import utils
import sys
from gsdmm import aaa

from celery_app.callback import callback
from pandas.core.frame import DataFrame
import datetime
import os
class EpollClusterHandler(tornado.web.RequestHandler):

    REDIS_EXPIRES = 1 * 24 * 60 * 60

    def options(self):
        # self.set_header('Access-Control-Allow-Credentials:true')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        # self.set_status(204)
        self.finish()

    # def get(self):
    #     # file = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    #     # read txt method three
    #     # f2 = open("./image/abc.txt", "r")
    #     # lines = f2.readlines()
    #     # for line3 in lines:
    #     #      print
    #     #     line3
    #     file_name = "1.txt"
    #     # self.set_header('Content-Type', 'application/octet-stream')
    #     # self.set_header('Access-Control-Allow-Origin', '*')
    #     # self.set_header('Access-Control-Allow-Origin:'.$_SERVER["HTTP_ORIGIN"]
    #     # self.set_header('Access-Control-Allow-Credentials:true')
    #     self.set_header('Access-Control-Allow-Headers', '*')
    #     self.set_header("Access-Control-Allow-Origin", "*")
    #     self.set_header('Access-Control-Allow-Methods', '*')
    #     # self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
    #     file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/cluster/")
    #     path = os.path.join(file_dir, file_name)
    #     print(path)
    #     with open(path) as f:
    #         self.set_header('Access-Control-Allow-Origin', '*')
    #         # self.write(str(f.readlines()).strip())
    #         # print(type(f.readlines()).__name__)
    #         # print(list(f.readlines()))
    #
    #         nice = f.readlines()[0].replace("[","").replace("]","").split('&&')
    #         for  i  in range(len(nice)):
    #             # print(type(nice(i)))
    #             nice[i] = tuple(eval(nice[i]))
    #         self.write({"data":nice})
    #     self.finish()
    #         # for line in f.readlines():
    #         #     print(line)
    #     f.close()


    @tornado.gen.coroutine
    def post(self, *args, **kwargs):

        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        try:
            json_params = json.loads(str(self.request.body, encoding='utf-8'))
            params = json_params.get('params','0')
            end = params.get('end','0')
            start = params.get('start', '0')
            # data_type = params.get('type', '0')
            # # upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
            # # os.remove(upload_path + "/output.xls")
            # print(start)
            # print(end)
            file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
            # # print(file_dir)
            # redis_cli = utils.get_redis_cli()
            path = os.path.join(file_dir, "output.xls")
            if os.path.isfile(path):
                print(path)
                dd = pd.read_excel(path)
                nice = [[i, j] for i, j in zip(dd.word, dd.frequence)]
                all_num = len(nice)
                nice = nice[int(start):int(end) + 1]

                path = os.path.join(file_dir, "count.xls")
                if os.path.isfile(path):
                    print(path)
                    dd = pd.read_excel(path)
                    nice2 = [[i, j] for i, j in zip(dd.iloc[:, 0], dd.iloc[:, 1])]
                    # self.finish({"data": nice, "data2" : nice2, "all_num": all_num})

                path = os.path.join(file_dir, "data.txt")
                if os.path.isfile(path):
                    try:
                        with open(path) as json_file:
                            json_data = json.load(json_file)
                            json_file.close()
                            self.set_header('Access-Control-Allow-Origin', '*')
                            self.finish({"data": nice, "data2": nice2,"data3": json_data, "all_num": all_num})
                            # return json_data
                    except FileNotFoundError:
                        print("No status table")
                        self.finish({"data": nice, "data2": nice2, "all_num": all_num})
                        # return {}
            else:
                self.set_header('Access-Control-Allow-Origin', '*')
                self.finish({"data": [], "data2":[],"all_num": 0})

                # self.finish()
        except Exception as e:
            print(e)



# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/epoll_cluster/*.*', EpollClusterHandler)
]

# Sept-12-2018  QiChang.Yin         create