# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
import ssl
from common import  cluster
import pandas as pd
import utils
from gsdmm.aaa import get_result
import datetime
from pandas.core.frame import DataFrame
import json
import os
class DataClusterHandler(tornado.web.RequestHandler):

    REDIS_EXPIRES = 1 * 24 * 60 * 60

    def options(self):
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        self.finish()
    #
    # def get(self):
    #     # file = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    #     # read txt method three
    #     # f2 = open("./image/abc.txt", "r")
    #     # lines = f2.readlines()
    #     # for line3 in lines:
    #     #      print
    #     #     line3
    #     file_name = "1.txt"
    #     # self.set_header('Content-Type', 'application/octet-stream')
    #     # self.set_header('Access-Control-Allow-Origin', '*')
    #     # self.set_header('Access-Control-Allow-Origin:'.$_SERVER["HTTP_ORIGIN"]
    #     # self.set_header('Access-Control-Allow-Credentials:true')
    #     self.set_header('Access-Control-Allow-Headers', '*')
    #     self.set_header("Access-Control-Allow-Origin", "*")
    #     self.set_header('Access-Control-Allow-Methods', '*')
    #     # self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
    #     file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/cluster/")
    #     path = os.path.join(file_dir, file_name)
    #     print(path)
    #     with open(path) as f:
    #         self.set_header('Access-Control-Allow-Origin', '*')
    #         # self.write(str(f.readlines()).strip())
    #         # print(type(f.readlines()).__name__)
    #         # print(list(f.readlines()))
    #
    #         nice = f.readlines()[0].replace("[","").replace("]","").split('&&')
    #         for  i  in range(len(nice)):
    #             # print(type(nice(i)))
    #             nice[i] = tuple(eval(nice[i]))
    #         self.write({"data":nice})
    #     self.finish()
    #         # for line in f.readlines():
    #         #     print(line)
    #     f.close()


    # @tornado.gen.coroutine
    def post(self, *args, **kwargs):

        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        try:
            json_params = json.loads(str(self.request.body, encoding='utf-8'))

            params = json_params.get('params','0')
            end = params.get('end','0')
            start = params.get('start', '0')
            type_data = json_params.get('type', '0')
            print("sdadsadsadasfsdfsda")

            file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
            # redis_cli = utils.get_redis_cli()

            # print("start={} end={} file_dir={} cluster_status={} file_type={} ".format(start,end,file_dir,redis_cli.get("cluster_status"),type_data))

            # print(end)
            # print(file_dir)
            # print(redis_cli.get("cluster_status"))
            # print(redis_cli.get("file_type"))

            # if redis_cli.get("cluster_status") == "prepare":
            # pid = os.fork()
            upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
            pid = 0
            if pid < 0:
                print("create process failed")
            elif pid == 0:

                print("this is child process")
                # os.system("nohup python3  /Users/yinqichang/Project/huazhu_data_mining/common/cluster.py  /Users/yinqichang/Project/huazhu_data_mining/static/upload/input.xlsx &")
                # redis_cli.set("cluster_status", "begin", ex=self.REDIS_EXPIRES)
                # callback.delay()
                # redis_cli.set("cluster_status", "begin", ex=self.REDIS_EXPIRES)
                # cluster_dir = os.path.join(os.path.dirname(__file__), '../static/cluster_txt')
                # IDF = pd.read_table(cluster_dir + "/chinese-idf-wordlist.txt", names=['word', 'number'],
                #       sep='\t').set_index('word').T.to_dict('records')[0]
                #
                # SYNONYM_LIST = pd.read_table(cluster_dir + "/syn_word_dup.txt",
                #                              names=['word', 'word_syn'], error_bad_lines=False).set_index('word').T.to_dict(
                #     'records')[0]
                print("============================================================")
                # dir_name = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
                # cluster_dataset = os.path.join(os.path.dirname(__file__), '../static/upload')  # 文件的暂存路径
                # cluster_dataset_path = upload_path + "/input.xlsx" if redis_cli.get("file_type") else upload_path + "/input.csv"
                # cluster_dataset_path = upload_path + "/input.xlsx"
                if os.path.isfile(os.path.join(upload_path,"input.xlsx")):
                    cluster_dataset_path = upload_path + "/input.xlsx"
                elif os.path.isfile(os.path.join(upload_path,"input.xls")):
                    cluster_dataset_path = upload_path + "/input.xls"
                else:
                    cluster_dataset_path = upload_path + "/input.csv"

                # cluster_dataset_path = upload_path + "/input.xls" if isNotCvs else upload_path+"/input.csv"
                print(upload_path,cluster_dataset_path)
                print("输入文件路径 : {}".format(cluster_dataset_path))
                print("输出文件路径 : {}".format(upload_path + "/output.xls"))
                try:
                    if str(type_data) == '0':
                        print("00000000000")
                        # dic = cluster.question_cluster(cluster_dataset_path, IDF, SYNONYM_LIST)
                        # dic.to_excel(upload_path + '/output.xls', sheet_name='output', index=False)
                        starttime = datetime.datetime.now()
                        try:
                            # os.remove(upload_path + "/count.xls")
                            # os.remove(upload_path+"/output.xls")
                            # os.remove(upload_path + "/data.txt")
                            length_sent, length_set, length_cluster, h3, length_ceshi_count, dic, repre ,result = cluster.question_cluster(
                                cluster_dataset_path)
                            # length_sent, length_set, length_cluster, h3, length_ceshi_count, dic, repre, result = question_cluster(
                            #     cluster_dataset, IDF, SYNONYM_LIST)
                            print("dsadsasafsdafsdafsdafsdaf")

                            data = DataFrame(repre)
                            print("dsadsasafsdafsdaf44444sdafsdaf")
                            dic.to_excel(upload_path + "/output.xls", index=False)
                            # data.to_excel(upload_path + "/reper111.xls", index=False)
                            print("dsadsasafsdafsdaf44444sddsdsddssdafsdaf")
                            endtime = datetime.datetime.now()
                            length_time = (endtime - starttime).seconds
                            count_dic = {}
                            count_dic['最小类元素的个数'] = 5
                            count_dic['聚类时间'] = length_time
                            count_dic['总共数据量'] = length_sent
                            count_dic['去重后条数'] = length_set
                            count_dic['共聚出条数'] = length_cluster
                            count_dic['有意义的类数'] = length_cluster - 1
                            count_dic['有意义的类中问题频率和'] = h3
                            count_dic['有意义的类中问题去重后'] = length_ceshi_count
                            count_dic['无意义类数'] = 1
                            count_df = pd.DataFrame([count_dic])
                            count_df = count_df.T
                            print("dsadsasafsdafsdaf44444sddsdsddssdafsdaf8888")
                            count_df.to_excel(upload_path + "/count.xls",header=False)
                            try:
                                with open(upload_path+'/data.txt', 'w+') as json_file:
                                    json_file.seek(0, 0)
                                    json_file.truncate()
                                    json_file.write(json.dumps(result, ensure_ascii=False, indent=4, sort_keys=True))
                                    json_file.close()
                                    # return True
                                    print("this is parent process")
                                    self.set_header('Access-Control-Allow-Origin', '*')
                                    self.finish({"data": [], "data2": [], "all_num": 0})
                            except IOError:
                                # return False
                                print(e)

                            # with open(upload_path + '/data.txt', 'w') as json_file:
                            #     # json_file.write()
                            #     json.dump(result, json_file, ensure_ascii=False)
                        except Exception as e:
                            print(e)
                        # redis_cli.set("cluster_status", "finish", ex=self.REDIS_EXPIRES)
                    else:
                        get_result(cluster_dataset_path, upload_path + '/output.xls')
                        # redis_cli.set("cluster_status", "finish", ex=self.REDIS_EXPIRES)
                except Exception as e:
                    print(e)
                    # redis_cli.set("cluster_status", "prepare", ex=self.REDIS_EXPIRES)

            else:
                print("this is parent process")
                self.set_header('Access-Control-Allow-Origin', '*')
                self.finish({"data": [], "data2": [], "all_num": 0})
            # else:
            #     if redis_cli.get("cluster_status") == "finish":
            #         path = os.path.join(file_dir, "output.xls")
            #         if os.path.isfile(path):
            #             print(path)
            #             dd = pd.read_excel(path)
            #             nice = [[i, j] for i, j in zip(dd.word, dd.frequence)]
            #             all_num = len(nice)
            #             nice = nice[int(start):int(end) + 1]
            #         path = os.path.join(file_dir, "count.xls")
            #         if os.path.isfile(path):
            #             print(path)
            #             dd = pd.read_excel(path)
            #             nice2 = [[i, j] for i, j in zip(dd.iloc[:, 0], dd.iloc[:, 1])]
            #             self.set_header('Access-Control-Allow-Origin', '*')
            #             self.write({"data": nice, "data2" : nice2, "all_num": all_num})
            #     else:
            #         self.set_header('Access-Control-Allow-Origin', '*')
            #         self.write({"data": [], "data2":[],"all_num": 0})

        except Exception as e:
            print(e)


# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/data_cluster/*.*', DataClusterHandler)
]

# Sept-12-2018  QiChang.Yin         create