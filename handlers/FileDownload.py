# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json


class FileDownloadHandler(tornado.web.RequestHandler):

    def get(self, *args, **kwargs):

        dir_name = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
        file_path = dir_name + "/output.xls"
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % file_path)
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')
        with open(file_path, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                self.write(data)
        self.set_header('Access-Control-Allow-Origin', '*')
        self.finish()

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/download/*.*', FileDownloadHandler)
]

# Sept-12-2018  QiChang.Yin         create