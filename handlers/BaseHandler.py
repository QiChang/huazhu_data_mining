# -*- coding: UTF-8 -*-
import json
import traceback
import tornado.web

from common import wulai_logger as logger
from common.wulai_constant import DEFAULT_ERROR_CODE, DEFAULT_ERROR_MSG
from common.wulai_exception import WulaiException, InvalidParamsException


class BaseHandler(tornado.web.RequestHandler):

    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)
        self.params = {}

    def check_origin(self, origin):
        return True

    def check_headers(self):
        # headers = self.request.headers
        # key = headers.get('Api-Auth-Key')
        # timestamp = headers.get('Api-Auth-Timestamp')
        # if not key or not timestamp:
        #     raise InvalidHeadersException
        return True

    def write_error(self, status_code, **kwargs):
        if status_code in [403, 404, 500, 503]:
            logger.error('status code: {}'.format(status_code))
            err_info = {
                u'code': DEFAULT_ERROR_CODE,
                u'msg': DEFAULT_ERROR_MSG
            }
            if "exc_info" in kwargs:
                exc_info = kwargs.get('exc_info')
                _, exc, trace_info = exc_info
                if isinstance(exc, WulaiException):
                    err_info['code'] = exc.get_error_code()
                    err_info['msg'] = exc.get_error_msg()
                    logger.error(u"code: {}, msg: {}".format(err_info['code'], err_info['msg']))
                else:
                    logger.error(exc)
                    logger.error(traceback.format_exc())
            self.finish(err_info)

    def parse_arguments(self, **arg_keys):
        req_params = self.request.body
        param_d = {}
        if req_params:
            try:
                param_d = json.loads(req_params)
            except Exception as e:
                pass

        for key, flag in arg_keys.items():
            item = param_d.get(key)
            if item is None:
                try:
                    item = self.get_argument(key)
                except Exception as e:
                    if flag:
                        error_detail = u'%s is missing' % key
                        raise InvalidParamsException(msg=error_detail)
            self.params[key] = item
        return None

    def get_weapp_argument(self, key, default=''):
        val = self.params.get(key, default)
        if val is None:
            val = default
        return val

    def get_raw_params(self):
        return self.params