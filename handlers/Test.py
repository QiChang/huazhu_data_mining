# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
import asyncio
import time

class TestHandler(tornado.web.RequestHandler):


    async def get(self):
       print("BBB")
       await asyncio.sleep(100)
       print("AAAA")
       self.finish({"OK":"result"})

    async def post(self):
       print("BBB")
       await asyncio.sleep(100)
       print("AAAA")
       self.finish({"OK":"result"})

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/test/*.*', TestHandler)
]

# Sept-12-2018  QiChang.Yin         create