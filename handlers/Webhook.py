# -*- coding: UTF-8 -*-
import json
from handlers.BaseHandler import BaseHandler
from common import wulai_reply, wulai_logger as logger
from common import config
from common import wulai_reply
import traceback

class WebhookHandler(BaseHandler):
    def get(self, *args, **kwargs):
        logger.info('Webhook Handler Get')
        self.write("It works")

    def post(self, *args, **kwargs):
        logger.info(' in WebhookHandler  '.center(80, '-'))
        params = json.loads(self.request.body)
        # logger.info('[params]:\t%s' % params)
        if config.debug:
            logger.pretty_json(params)
        resp = {
            'suggested_response': params['bot_response']['suggested_response']
        }
        try:
            resp['suggested_response'] = wulai_reply.get_reply(params)
        except Exception:
            msg = traceback.format_exc()
            logger.error(msg)
        # resp.setdefault('suggested_response', {})
        # logger.info('[response]:\t%s' % resp)
        if config.debug:
            logger.pretty_json(resp)
        logger.info(' leave WebhookHandler  '.center(80, '-'))
        self.write(resp)

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/webhook%s" % path_regex, WebhookHandler),
    (r"/v1/webhook", WebhookHandler),
]

# Sept-12-2018  QiChang.Yin         created.