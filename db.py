# -*- coding: UTF-8 -*-
from redis import StrictRedis
import json
import conf
from common import wulai_logger as logger

r = StrictRedis(host=conf.REDIS_HOST, port=conf.REDIS_PORT, password=conf.REDIS_PWD,ssl=False, db=conf.REDIS_DB)
PRE_KEY = 'PLUGIN_rongyun_cloud'
SECRET_KEY = "PLUGIN_SECRET"
MSG_KEY = 'MSG_WX'
CLIENT_KEY = 'PLUGIN_CONFIG'
CLIENT_KEY_2 = 'PLUGIN_CONFIG_2'
APP_LIST_KEY = 'APP_LIST'


def get_token(user_id):
    result = {}
    token = r.get("{}_{}".format(PRE_KEY, user_id))
    try:
        if token:
            result = json.loads(token)
    except Exception as e:
        logger.error(e)
    return result


def set_token(user_id, token):
    r.set("{}_{}".format(PRE_KEY, user_id), json.dumps(token), ex=60*60)


def get_secret(pubkey, env):
    secret = r.get("{}_{}_{}".format(SECRET_KEY, env, pubkey))
    if secret:
        secret = secret.decode()
    return secret


def set_secret(pubkey, env, secret):
    return r.set("{}_{}_{}".format(SECRET_KEY, env, pubkey), secret)


def pub_msg(user_id, msg):
    r.rpush("{}_{}".format(MSG_KEY, user_id), json.dumps(msg))

    
def sub_msg(user_id):
    msg = r.lpop("{}_{}".format(MSG_KEY, user_id))
    return msg


def set_config(client, data):
    return r.set("{}_{}".format(CLIENT_KEY, client), json.dumps(data), ex=30)


def get_config(client):
    result = {}
    config = r.get("{}_{}".format(CLIENT_KEY, client))
    try:
        if config:
            result = json.loads(config)
    except Exception as e:
        logger.error(e)
    return result


def set_config2(client, data):
    return r.set("{}_{}".format(CLIENT_KEY_2, client), json.dumps(data), ex=60 * 60 * 24)


def get_config2(client):
    result = {}
    config = r.get("{}_{}".format(CLIENT_KEY_2, client))
    try:
        if config:
            result = json.loads(config)
    except Exception as e:
        logger.error(e)
    return result


def set_app_list(data):
    return r.set("{}".format(APP_LIST_KEY), json.dumps(data), ex=60*60)


def get_app_list():
    result = {}
    app_list = r.get("{}".format(APP_LIST_KEY))
    try:
        if app_list:
            result = json.loads(app_list)
    except Exception as e:
        logger.error(e)
    return result