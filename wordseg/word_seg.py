#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 13:55:35 2018

@author: liujinyan
"""

#!/bin/env python
#-*- encoding: utf-8 -*-

import grpc
import sys
import json
import threading
G_DEP_DIR = "./nlpdep_2/"
sys.path.append(G_DEP_DIR)
G_DEP_DIR = "bin/nlpdep_2/"
sys.path.append(G_DEP_DIR)
# from nlp_interface_pb2_grpc import NLPGrpcServiceStub
# from nlp_interface_pb2 import NlpTokenizeRequest
# from nlp_interface_pb2 import NlpBatchTokenizeRequest
# from nlp_interface_pb2 import NlpAppWordBundle
# from nlp_interface_pb2 import NlpQueryBundle
# from nlp_interface_pb2 import QuestionTypeRecongnizeRequest
#conf = __import__('conf')

class WordSegService(object):
    
    def __init__(self):
        grpc_address = conf.g_fasttext_cfg['word_seg_host']
        self.__channel = grpc.insecure_channel(grpc_address)
        self.__stub = NLPGrpcServiceStub(self.__channel)    


    def getQuestionTypeRecognize(self, query):
        try:
            response = self.__stub.QuestionTypeRecongnize(QuestionTypeRecongnizeRequest(msg=query))
            type_rec = response.cate+'_'+response.qfocus
        except Exception as e:
            error_line = sys.exc_info()[2].tb_lineno
            type_rec='_'
        return type_rec



    def getWordSegResult(self, query, place_holder='', msg_id=1):
        try:
            wordSegRes = dict()
            wordSegRes['tokens'] = []
            wordSegRes['phrases'] = []
            wordSegRes['search_tokens'] = []
            param_query = NlpQueryBundle(query = query)
            rquest_list = NlpTokenizeRequest(msg_id = msg_id, query = param_query, place_holder = '')
            response = self.__stub.NlpTokenize(rquest_list)
            
            for token in response.tokens:
                curr_dict = dict()
                curr_dict["value"] = token.value
                curr_dict["weight"] = token.weight
                curr_dict["pos_tag"] = token.pos_tag
                wordSegRes['tokens'].append(curr_dict)
            
            for phrase in response.phrases:
                curr_dict = dict()
                curr_dict["value"] = phrase.value
                curr_dict["weight"] = phrase.weight
                curr_dict["pos_tag"] = phrase.pos_tag
                wordSegRes['phrases'].append(curr_dict)
                
            #for search_token in response.search_tokens:
            #    curr_dict = dict()
            #    curr_dict["value"] = search_token.value
            #    curr_dict["weight"] = 0
            #    curr_dict["pos_tag"] = 0
            #    wordSegRes['search_tokens'].append(curr_dict)
        except Exception as e:
            error_line = sys.exc_info()[2].tb_lineno
            print ('line=%s,'%error_line,e)
        return wordSegRes
    
    def getBatchWordSegResult(self, querys, place_holder='', msg_id=1):
        try:
            bacthWordSegRes = dict()
            app_word = [NlpAppWordBundle(word = '')]

            param_querys = []
            for query in querys:
                param_querys.append(NlpQueryBundle(query = query, app_words = app_word))
            rquest_list = NlpBatchTokenizeRequest(msg_id = msg_id, querys = param_querys, place_holder = '')
            response = self.__stub.NlpBatchTokenize(rquest_list)
            count = 0
            for curr in response.query_tokens:
                count += 1
                bacthWordSegRes[count] = dict()
                bacthWordSegRes[count]['tokens'] = []
                bacthWordSegRes[count]['phrases'] = []
                bacthWordSegRes[count]['search_tokens'] = []
                for token in curr.tokens:
                    curr_dict = dict()
                    curr_dict["value"] = token.value
                    bacthWordSegRes[count]['tokens'].append(curr_dict)
                for phrase in curr.phrases:
                    curr_dict1 = dict()
                    curr_dict1["value"] = phrase.value
                    curr_dict1["weight"] = phrase.weight
                    curr_dict1["pos_tag"] = phrase.pos_tag
                    bacthWordSegRes[count]['phrases'].append(curr_dict1)
                for search_token in curr.search_tokens:
                    curr_dict2 = dict()
                    curr_dict2["value"] = search_token.value
                    bacthWordSegRes[count]['search_tokens'].append(curr_dict2)
        except Exception as e:
            error_line = sys.exc_info()[2].tb_lineno
            print ('line=%s,'%error_line,e)
        return bacthWordSegRes
def test():
    ws = WordSegService()
    query = '长春市长春药店'
    res = ws.getWordSegResult(query)
    print (json.dumps(res))
    
if '__main__' == __name__:
    test()
