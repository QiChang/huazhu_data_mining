#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import json
import sys
import time
from collections import Counter, defaultdict
import urllib
import requests
import numpy as np
import random
# from gensim import models
# from gensim import corpora
# import jieba
# import sklearn.metrics
# import word_seg
#import front_end_cache
# from .word_seg import WordSegService

import time
#import doc_util
import pandas as pd
import collections
from logging  import Logger
log=Logger("liujinyan")
import sys


# IDF = pd.read_table('/Users/liujinyan/Desktop/formal/chinese-idf-wordlist.txt', 
#    names=['word', 'number'],sep='\t').set_index('word').T.to_dict('records')[0]
import csv
import codecs
import xlwt
import types

    
def cluster_all_dict(synony_list,sentence):
    """
        将每一句话拥有相同的词进行聚类，且每一类中元素的个数应该大于5个
        :param synony_list:
        :param content:
        :param min_samples:
        :return:
        """
    synony_tup= [[item[0] for item in i] for i in synony_list]
    #排序
    total_word = ['|'.join(i) for i in synony_tup]
    all_total = pd.DataFrame(columns=['total_word', 'sentence'])
    all_total['total_word'] = total_word
    all_total['content'] = sentence
    all_num = all_total.groupby('total_word').content.count()
    all_total_dict = all_total.groupby('total_word').content.apply(list).to_dict()
    set_index1 = all_num[all_num > 4].index
    set_index1 = [i for i in set_index1 if len(i) > 0]
    cluster_dict = {}
    for i in set_index1:
        cluster_dict[i] = all_total_dict[i]
                #logger.get().debug('cluster_dict for clustering')
    return cluster_dict, all_num, all_total_dict, total_word


def data_write_csv(file_name, datas):#file_name为写入CSV文件的路径，datas为要写入数据列表
    file_csv = codecs.open("/Users/yinqichang/Project/huazhu_data_mining/static/download/1.csv",'w','utf-8')#追加
    writer = csv.writer(file_csv, delimiter=' ', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
    for data in datas:
        writer.writerow(data)
    print("保存文件成功，处理结束")


def data_write_cluster_csv(file_name, datas):#file_name为写入CSV文件的路径，datas为要写入数据列表
    file_csv = codecs.open("/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.csv",'w','utf-8')#追加
    writer = csv.writer(file_csv, delimiter=' ', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
    for data in datas:
        writer.writerow(data)
    print("保存文件成功，处理结束")

def text_save(filename, data):#filename为写入CSV文件的路径，data为要写入数据列表.
    # file_csv = codecs.open("/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.csv", 'w', 'utf-8')  # 追加
    filename = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    file = open(filename,'a')
    for i in range(len(data)):
        print(data)
        s = str(data[i]).replace('[','').replace(']','')#去除[],这两行按数据不同，可以选择
        s = s.replace("'",'').replace(',','') +'\n'   #去除单引号，逗号，每行末尾追加换行符
        file.write(s)
    file.close()
    print("保存文件成功")


def data_write(file_path, datas):
    f = xlwt.Workbook()
    sheet1 = f.add_sheet(u'sheet1', cell_overwrite_ok=True)  # 创建sheet

    # 将数据写入第 i 行，第 j 列
    i = 0
    for data in datas:
        print(data)
        for j in range(len(data)):
            sheet1.write(i, j, str(data[j]))
        i = i + 1

    f.save(file_path)  # 保存文件


def data_write_exls(file_name,datas):
    wbk = xlwt.Workbook()
    sheet = wbk.add_sheet("myNumbers")  # Excel页签标识，可以随便写
    #
    # obj = open("myNumbers.txt", "r")  # 列表里的数据放到了一个 txt文件中
    # all_txt = obj.read()
    # new_txt = eval(all_txt)
    # # print all_txt
    # # print new_txt
    #
    i = 0
    datas = list(datas)
    for netted in datas:
        for x, item in enumerate(netted):
            # if type(item) is types.StringType:
            # item = item
            sheet.write(i, str(x), item)
            # sheet.write(i, x, item)
        i += 1
    wbk.save("/Users/yinqichang/Project/huazhu_data_mining/static/download/1.xls")

def question_cluster(cluster_dataset,min_samples=5):
    t = 'v'
    if t == 'v':
        import random
        database = pd.read_excel(cluster_dataset)
        ws = database[u'问题']
        result =  [[(random.choice(ws), random.randint(1, 100)) for j in range(i)] for i in range(20)]
        print(result)
        # print(type(result))
        # data_write_csv("1", result)
        # text_save("1",result)
        # data_write_cluster_csv('1',list(result))
        # data_write("/Users/yinqichang/Project/huazhu_data_mining/static/download/1.xlsx",result)
        return result
    
#     # 找每一句话中idf>3的词，如果有的词找不到idf就=16
#     if min_samples == 5 :
#         fre_num=10
#         fre_num_random=20
#     elif min_samples == 3 :
#         fre_num=20
#         fre_num_random=50
#     else:
#         fre_num=50
#         fre_num_random=100
#
#     sentence = []
#     database = pd.read_excel(cluster_dataset)
#     sentence=database[u'问题'][1:300]
#     #logger.get().debug('enter _question_cluster2')
#     #logger.get().debug('shape of dataset is %s' % len(sentence))
#     tokens = []
#     #logger.get().debug('take tokeng')
#     for k_question in sentence:
#         ws = WordSegService()
#         ws_res = ws.getWordSegResult(k_question)
#         res = [phrase['value'] for phrase in ws_res['phrases']]
# #             logger.get().info('cluster_seg_res:%s' % '|'.join(res))
#         tokens.append(res)
#     idf_vocab=[]
#     for w in tokens:
#         vocab_dict = {}
#         vocab_dict_new={}
#         for vocal in w:
#             if vocal.strip():
#                 x = int(IDF.get(vocal, 16))
#                 if x > 3:
#                     vocab_dict[vocal] = x
#         idf_vocab.append(vocab_dict)
#     sort_dict=[sorted(i.items(),key = lambda x:x[1],reverse = True) for i in  idf_vocab]
#     cluster_dict_first, all_num, all_total_dict, total_word = cluster_all_dict(sort_dict,sentence)
#     #logger.get().debug('first_cluster')
#     # 将每一类中相似问少于5个的词  中idf最小的删除
#     set_index2 = all_num[all_num < 4].index
#     choose_all_total_1 = {}
#     for i in set_index2:
#         choose_all_total_1[i] = all_total_dict[i]
#     synony_list_new =sort_dict[:]
#     for i, v in enumerate(total_word):
#         if v not in set_index2:
#             synony_list_new[i] = []
#     synony_list_new=[[i[j] for j in range(len(i)-1)] if len(i) > 1 else i for i in synony_list_new]
#     # 在聚类
#     cluster_dict_second, all_num_second, all_total_dict_second, total_word_second = cluster_all_dict(synony_list_new, sentence)
#     #logger.get().debug('second_cluster')
#
#
#     # 把具有相同key（类名字） 的元素合并
#     dic = {}
#     for key in cluster_dict_first:
#         if cluster_dict_second.get(key):
#             dic[key] = cluster_dict_first[key] + cluster_dict_second[key]
#         else:
#             dic[key] = cluster_dict_first[key]
#     for key in cluster_dict_second:
#         if cluster_dict_first.get(key):
#             pass
#         else:
#             dic[key] = cluster_dict_second[key]
#     #logger.get().debug('combind_dict')
#     #对大于fre_num的找需求词
#     high_dic={}
#     value_list=[]
#     key_list=[]
#     for key,value in dic.items():
#         if 5 <=len(value)<=fre_num:
#             high_dic[key]=value
#         else:
#             value_list.append(value)
#             key_list.append(key)
#     #logger.get().debug('bigger_5')
#
#
#     ws = WordSegService()
#     demand=[[ws.getQuestionTypeRecognize(item) for item in i] for i in value_list]
#     #logger.get().debug('demandr')
#     # 按需求词groupby
#     all_group=[]
#     for i in range(len(demand)):
#         df =pd.DataFrame(columns=('demand','value_list'))
#         df['demand']=demand[i]
#         df['value_list']=value_list[i]
#         grouped = df.groupby('demand').value_list.apply(list)
#         all_group.append(grouped)
#     index_demand=[all_group[i].index for i in range(len(all_group))]
#
#     # 更改名字
#     all_list=[]
#     for i,v in enumerate(key_list):
#         sd=[]
#         for j in index_demand[i]:
#             new_key= unicode(v)+'|' + unicode(j)
#             sd.append(new_key)
#         all_list.append(sd)
#     all_value=[[j for j in all_group[i]] for i in range(len(all_group))]
#     key_sum=[j for i in all_list for j in i]
#     value_sum=[j for i in all_value for j in i]
#
#
#
#     dic_other={}
#     for i in range(len(key_sum)):
#         dic_other[key_sum[i]]= value_sum[i]
#     # 对剩下的降序排列抽取一定的数量
#     middle_dic={}
#     small_other={}
#     for key,value in dic_other.items():
#         if  len(value)<5:
#             pass
#         elif 5 <=len(value)<=fre_num_random:
#             middle_dic[key]=value
#         else:
#             small_other[key]=value
#
#
#     by_value =sorted(small_other.items(),key = lambda item:len(item[1]),reverse = True)
#     small_dic={}
#     for item  in by_value:
#         small_dic[item[0]]=random.sample(item[1], fre_num_random) if len(item[1])>fre_num_random else item[1]
#
#     dict_all = high_dic.items()+  middle_dic.items() + small_dic.items()
#
#
#     cluster=[value for key,value in dict_all]
#     represent_questions=[key for key,value in dict_all]
#
#     repre =[]
#     clusters=[]
#     for list_que in cluster:
#         sort_word = sorted(list_que,key = lambda i:len(i),reverse=False)
#         repre.append(sort_word[len(sort_word)//2])
#         clusters.append(sort_word)
#
#     represent_questions=[repre[i]+'  ' +'('+represent_questions[i]+')' for i in  range(len(represent_questions))]
#     obj = collections.Counter('clusters')
#
#
#     #logger.get().debug(len(represent_questions))
#     res = [[(y, 1) for y in x] for x in clusters]
#     return res, clusters
#




if __name__  == "__main__":
    cluster_dataset=sys.argv[1]
    min_samples =sys.argv[2]
    import json

    filename = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    file = open(filename, 'a')
    res = question_cluster(cluster_dataset,min_samples)
    # file.write("[")
    count_len = len(res)
    count = 0
    for m in res:
        for n in m:
            # print (n[0], " ", n[1])
            # print("{}{}{}".format(n[0]," ", n[1]))
            file.write("(\"{}\"{}\"{}\")&&".format(n[0],",", n[1]).replace("\n",""))
        # print("\n({},{}{})".format(" "," ", " "))
        if count < count_len - 1:
            file.write("(\"{}\"{}\"{}\")&&".format("", ",", "").replace("\n",""))
        else:
            file.write("(\"{}\"{}\"{}\")".format("", ",", "").replace("\n",""))
        count += 1

    # file.write("(\"{}\"{}\"{}\")".format("", ",", "").replace("\n", ""))
    # file.write("")
    # file.write("]")
    # filename = "/Users/yinqichang/Project/huazhu_data_mining/static/cluster/1.txt"
    # file = open(filename, 'a')
    # for i in range(len(data)):
    #     print(data)
    #     s = str(data[i]).replace('[', '').replace(']', '')  # 去除[],这两行按数据不同，可以选择
    #     s = s.replace("'", '').replace(',', '') + '\n'  # 去除单引号，逗号，每行末尾追加换行符
    #     file.write(s)
    # file.close()
    # print("保存文件成功")