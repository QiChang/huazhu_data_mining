# -*- conding:utf-8 -*-

from gsdmm.gsdmm import MovieGroupProcess
import pandas as pd 
import jieba


def compute_V(texts):
    V = set()
    for text in texts:
        for word in text:
            V.add(word)
    return len(V)

def get_result(fpath = '/Users/taindongpo/works/huazhu/huazhu_data.xlsx',outpath = '/Users/taindongpo/works/huazhu/huazhu_data222.xlsx',K = 800):
    data = pd.read_excel(fpath)

    data_unique = data['问题'].value_counts().reset_index()
    data_unique.columns = ['query','frequence']
    data2 = data_unique[data_unique['query'].str.len()>1]

    x = [jieba.lcut(str(i)) for i in data_unique['query']]

    mgp = MovieGroupProcess(K=K, alpha=0.1, beta=0.1, n_iters=30)

    y = mgp.fit(x,compute_V(x))

    data2['cluster_label'] = y
    em_df = pd.DataFrame({'query':[' '],'frequence':[' '],'cluster_label':[' ']})
    a = pd.concat([data2[data2.cluster_label == i].reset_index(drop = True).append(em_df) for i in data2.cluster_label.unique().tolist()]).reset_index(drop=True)

    a[['query','frequence']].to_excel(outpath,index=False)
    print('Finish!')
    # return a[['query','frequence']]