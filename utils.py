import os
import re
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.utils import parseaddr, formataddr
import multiprocessing
import config

from redis import StrictRedis

EMAIL_REGEX = "^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$"
DEBUG = config.debug
EMAIL_SEND_TO = ["wangqisheng@laiye.com"] if DEBUG else ['liuruojuan@laiye.com', 'chao@laiye.com']
EMAIL_CC_LIST = ["wangqisheng@laiye.com"] if DEBUG else ['liuruojuan@laiye.com', 'chao@laiye.com']


def get_redis_cli():
    return StrictRedis(host=config.REDIS_HOST, port=config.REDIS_PORT, password=config.REDIS_PWD,
                       ssl=True, decode_responses=True)


class RegexHandler(object):

    @staticmethod
    def is_invalid_email(email_address):
        return re.match(EMAIL_REGEX, email_address)

    @staticmethod
    def is_invalid_phone(phone_number):
        if '-' in phone_number:
            phone_number = phone_number.replace('-', '')
            length = len(phone_number)
            return length == 11 or length == 12
        else:
            length = len(phone_number)
            return length == 11 or length == 8



if __name__ == "__main__":
    pass
