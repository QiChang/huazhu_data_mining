#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import numpy as np
import pandas as pd
import jieba 
import jieba.analyse
import  sys
from conf.constant import IDF, SYNONYM_LIST
from collections import Counter

from pandas.core.frame import DataFrame
import datetime
import os
starttime = datetime.datetime.now()
# print("11111111")
# upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/cluster_txt")
# # upload_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '/static/cluster_txt')
# print(upload_path)
# IDF = pd.read_table("./chinese-idf-wordlist.txt", names=['word', 'number'],
#                     sep='\t').set_index('word').T.to_dict('records')[0]
# print("22222222")
# # print()
# SYNONYM_LIST = pd.read_table("./syn_word_dup.txt",
#                              names=['word', 'word_syn'], error_bad_lines=False, sep=' ').set_index(
#     'word').T.to_dict('records')[0]
#
# print("43333333")

def stopwordslist(filepath):  
    stopwords = [line.strip() for line in open(filepath, 'r').readlines()]  
    return stopwords

upload_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static/cluster_txt')
print(upload_path)
# upload_path = os.path.join(os.path.dirname(__file__), "static/cluster_txt")
print(upload_path)
stopwords = stopwordslist(upload_path+"/stopword.txt")
punctuations = list(set([ u'，', u'。', u'、', u'：', u'(',u')', u'[', u']', u'.', u',', u' ', u'\u3000', u'”', u'“',u'？', u'?', u'！', u'‘', u'’', u'…', '!', '+', '-', '*','#', '"', "'", '^', ':', '/', '%', '=', ';', u'；', '@','{', '}', u'」', u'「', u'．', u'—', u'－', u'『', u'』',u'□', u'【', u'】', u'◆', u'（', u'）', u'·', u'`', u'·',u'\t', u'\\n', u'~', u'《', u'》', u'◎']))

def tokenlist(content):
    tokens = []
    for i  in content:
        o = jieba.cut(i)
        a = filter(lambda t: t not in punctuations, o)
        #a = [tk for tk in a if not tk.isdigit()]
        a = filter(lambda t: t not in stopwords, a)
        try:
            a=list(a)  
        except:
            a=[]
        tokens.append(a)
    return tokens 



def cluster_all_dict(synony_list,sentence):
    """
        将每一句话拥有相同的词进行聚类，且每一类中元素的个数应该大于5个
        :param synony_list:
        :param content:
        :param min_samples:
        :return:
        """
    synony_tup= [[item[0] for item in i] for i in synony_list]
    #排序
    total_word = ['|'.join(i) for i in synony_tup]
    all_total = pd.DataFrame(columns=['total_word', 'sentence'])
    all_total['total_word'] = total_word
    all_total['content'] = sentence
    all_num = all_total.groupby('total_word').content.count()
    all_total_dict = all_total.groupby('total_word').content.apply(list).to_dict()
    set_index1 = all_num[all_num > 4].index
    set_index1 = [i for i in set_index1 if len(i) > 0]
    cluster_dict = {}
    for i in set_index1:
        cluster_dict[i] = all_total_dict[i]
                #logger.get().debug('cluster_dict for clustering')
    return cluster_dict, all_num, all_total_dict, total_word
def product_dict(high_dic):
    rr=[]
    for i in  range(len(high_dic)):
        a = [' 'for _ in range(len(high_dic[i][1])+1)]
        a[0]=high_dic[i][0]
        rr.append(a)
        con=[]
        for i in  range(len(high_dic)):
            a = [' 'for _ in range(len(high_dic[i][1])+1)]
            a[0:len(high_dic[i][1])]=high_dic[i][1]
            con.append(a)
        rr_new=[j for i in rr for j in i]
        con_new=[j for i in con for j in i]
        high_all=pd.DataFrame(columns=('name','content'))
        high_all['name']=rr_new
        high_all['content']=con_new
    return high_all

def question_cluster(cluster_dataset):
    print("&&&&&&&7")
    try:
        print("1&&&&&&&7")
        all_content=pd.read_excel(cluster_dataset,error_bad_lines=True)
        content=all_content['问题']
        print("2&&&&&&&7")
    except Exception as e:
        print(e)
        print("3&&&&&&&7")
        all_content=pd.read_csv(cluster_dataset,error_bad_lines=True)
        content=all_content['问题']
        print("4&&&&&&7")

    sentence = content
    sentence=list(set(sentence))
    tokens=tokenlist(sentence)
    idf_vocab=[]
    for w in tokens:
        vocab_dict = {}
        vocab_dict_new={}
        for vocal in w:
            if vocal.strip():
                x = int(IDF.get(vocal, 16))
                if x > 3:
                    vocab_dict[vocal] = x
        idf_vocab.append(vocab_dict)

    sort_dict=[sorted(i.items(),key = lambda x:x[1],reverse = True) for i in  idf_vocab]
    synony_list=[]
    for sd in sort_dict:
            e = [(SYNONYM_LIST.get(tuple_value[0], tuple_value[0]), tuple_value[1]) for tuple_value in sd]
            synony_list.append(e)

    cluster_dict_first, all_num, all_total_dict, total_word = cluster_all_dict(synony_list,sentence)
    set_index2 = all_num[all_num < 4].index
    choose_all_total_1 = {}
    for i in set_index2:
        choose_all_total_1[i] = all_total_dict[i]

    synony_list_new = synony_list[:]

    for i, v in enumerate(total_word):
        if v not in set_index2:
            synony_list_new[i] = []
                
    synony_list_new=[[i[j] for j in range(len(i)-1)] if len(i) > 1 else i for i in synony_list_new]

    cluster_dict_second, all_num_second, all_total_dict_second, total_word_second = cluster_all_dict(synony_list_new, sentence)
    dic = {}
    for key in cluster_dict_first:
        if cluster_dict_second.get(key):
            dic[key] = cluster_dict_first[key] + cluster_dict_second[key]
        else:
            dic[key] = cluster_dict_first[key]
    for key in cluster_dict_second:
        if cluster_dict_first.get(key):
            pass
        else:
            dic[key] = cluster_dict_second[key]
    
    clusters=[value for key,value in dic.items()]
    represent=[key for key,value in dic.items()]
    h_count=[]
    for i in clusters:
        for j in i :
            h_count.append(j)
    other = list(set(content)-set(h_count))
    digtal =[tk for tk in other if  str(tk).isdigit()]
    big_data=[i for i in other if len(str(i))>50]
    other_1 = list(set(other)-set(digtal)-set(big_data))
    clusters.append(digtal)
    clusters.append(big_data)
    ceshi_count=[]
    for i in  clusters:
        for j in i :
            ceshi_count.append(j)

    
    clusters.append(other_1)
    other_cluster=[]
    for i in clusters :
        for j in i :
            other_cluster.append(j)
        other_cluster.append('\n')
    word=Counter(content)
    p = [word[i] for i in other_cluster]
    all_dic=pd.DataFrame(columns=('word','frequence'))
    all_dic['word']=other_cluster
    all_dic['frequence']=p
    try:
        all_dic.frequence[all_dic['word']=='\n']=' '
        h1= all_dic.frequence.iloc[0:len(all_dic)-len(other_1)-1]
        h2 =[int (i) for i  in h1 if i !=' ']
        length_sent=len(content)
        length_set = len(sentence)
        length_cluster =len(clusters)
        length_ceshi_count =len(ceshi_count)
        h3 = sum(h2)
    except Exception as e:
        print(e)
    high_dic = sorted(dic.items(), key=lambda d: len(str(d[0]).split("|")), reverse=True)
    dict = {}
    for i in high_dic:
        dict[i[0]] = i[1]
    print(len(dict))

    result = {}
    result["treedata"] = []

    for k, value in dict.items():
        tempobj = {"label": k, "children": []}
        tempobj["children"] = []
        for j in value:
            tempobj["children"].append({"label": j})
        result["treedata"].append(tempobj)
    print(len(result["treedata"]))
    print("dassadsadsadsasdasd")

    #    high_all=product_dict(high_dic)
    return length_sent, length_set, length_cluster, h3, length_ceshi_count, all_dic, represent, result


if '__main__' == __name__:
    cluster_dataset=sys.argv[1]


    # print("11111111")
    # IDF = pd.read_table(upload_path+"/chinese-idf-wordlist.txt", names=['word', 'number'],
    #                     sep='\t').set_index('word').T.to_dict('records')[0]
    # print("22222222")
    # SYNONYM_LIST = pd.read_table(upload_path+"/syn_word_dup.txt",
    #                              names=['word', 'word_syn'], error_bad_lines=False, sep=' ').set_index(
    #     'word').T.to_dict('records')[0]
    #
    # print("43333333")
    length_sent,length_set,length_cluster,h3,length_ceshi_count,dic,repre,result=question_cluster(cluster_dataset)
    upload_path_1 = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
    data=DataFrame(repre)
    dic.to_excel(upload_path_1+"/data1222.xls",index =False)
    data.to_excel(upload_path_1+"/reper111.xls",index =False)
#     endtime = datetime.datetime.now()
#     length_time = (endtime - starttime).seconds
#     count_dic={}
#     count_dic['最小类元素的个数']=5
#     count_dic['聚类时间']=length_time
#     count_dic['总共数据量']=length_sent
#     count_dic['去重后条数']=length_set
#     count_dic['共聚出条数']=length_cluster
#     count_dic['有意义的类数']=length_cluster-1
#     count_dic['有意义的类中问题频率和']= h3
#     count_dic['有意义的类中问题去重后']=length_ceshi_count
#     count_dic['无意义类数']= 1
#     count_df= pd.DataFrame([count_dic])
#     count_df=count_df.T
#     count_df.to_excel(upload_path+"/count.xls",header=False)
# #    high_all.to_excel('./label.xls')
# #     print(dic)
# #     count_df=count_df.T
# #         count_df.to_excel('./count.xls',header=False)
#     import json
#     with open('data.txt','w') as json_file:
#         json.dump(result,json_file,ensure_ascii = False)
# #    high_all.to_excel('./label.xls')
#     print(result)
#     print('close')

    cluster_dataset = sys.argv[1]
    length_sent, length_set, length_cluster, h3, length_ceshi_count, dic, repre, result = question_cluster(
        cluster_dataset, IDF, SYNONYM_LIST)
    data = DataFrame(repre)
    # dic.to_excel('./data1222.xls', index=False)
    # data.to_excel('./reper111.xls', index=False)
    endtime = datetime.datetime.now()
    length_time = (endtime - starttime).seconds
    count_dic = {}
    count_dic['最小类元素的个数'] = 5
    count_dic['聚类时间'] = length_time
    count_dic['总共数据量'] = length_sent
    count_dic['去重后条数'] = length_set
    count_dic['共聚出条数'] = length_cluster
    count_dic['有意义的类数'] = length_cluster - 1
    count_dic['有意义的类中问题频率和'] = h3
    count_dic['有意义的类中问题去重后'] = length_ceshi_count
    count_dic['无意义类数'] = 1
    count_df = pd.DataFrame([count_dic])
    count_df = count_df.T
    count_df.to_excel(upload_path_1+'/count.xls', header=False)
    import json
    with open(upload_path_1+'/data.txt', 'w') as json_file:
        json.dump(result, json_file, ensure_ascii=False)
    #    high_all.to_excel('./label.xls')
    print(result)
    print('close')
