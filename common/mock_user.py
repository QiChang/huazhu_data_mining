# -*- coding:utf-8 -*-
import datetime


# Todo 体重增长是否正常


class MockUser(object):
    def __init__(self):
        self.init()

    def init(self):
        self.height = '1.68'
        self.weight_start = '51'  # 单位kg
        self.weight_now = '68'
        self.preg_weeks = 0  # 怀孕周数
        self.pre_production_time = '2018-11-25'  # 预产期
        self.incr_range = [0, 0]  # 孕期体重总增长量范围 单位kg
        self.BMI_start = float(self.weight_start) / pow(float(self.height), 2)
        self.incr_rate = 0
        self.BMI()
        self.disease_flag = False
        self.current_status = -1

    def BMI(self):
        v = self.BMI_start
        if v < 19.8:
            self.incr_range = [12.5, 18]
        elif 19.8 < v < 26:
            self.incr_range = [11.5, 16]
        elif 26 < v < 29.9:
            self.incr_range = [7, 11.5]
        elif 30 < v:
            self.incr_range = [6, 16]

    def incr_percent(self):
        w = self.preg_weeks
        if w <= 12 or 36 < w < 40:
            self.incr_rate = 1 / 12
        elif 12 < w <= 36:
            self.incr_rate = 10 / 12

    def cal_pred_weight(self):
        w = self.preg_weeks
        pred_w_range = [0, 0]
        if w <= 12:
            pred_w_range = [x * self.incr_rate * w / 12 for x in self.incr_range]
            print('pred_w_range_12', pred_w_range)
        elif 12 < w <= 36:
            pred_w_range_12 = [x * 1 / 12 for x in self.incr_range]
            pred_w_range_36 = [x * self.incr_rate * (w - 12) / 24 for x in self.incr_range]
            pred_w_range = [x + y for x, y in zip(pred_w_range_12, pred_w_range_36)]
            print('pred_w_range_36', pred_w_range_12, pred_w_range_36, pred_w_range)
        elif 36 < w <= 40:
            pred_w_range_12 = [x * 1 / 12 for x in self.incr_range]
            pred_w_range_36 = [x * 10 / 12 for x in self.incr_range]
            pred_w_range_40 = [x * self.incr_rate * (w - 36) / 4 for x in self.incr_range]
            pred_w_range = [x + y + z for x, y, z in zip(pred_w_range_12, pred_w_range_36, pred_w_range_40)]
            print('pred_w_range_40', pred_w_range_12, pred_w_range_36, pred_w_range_40, pred_w_range)
        print(pred_w_range[0], pred_w_range[1], int(self.weight_now) - int(self.weight_start))
        delta = int(self.weight_now) - int(self.weight_start)
        if pred_w_range[0] <= delta <= pred_w_range[1]:
            self.current_status = 0
            # return "体重正常"
        elif delta < pred_w_range[0]:
            self.current_status = 1
            # return "体重过轻"
        elif delta > pred_w_range[1]:
            self.current_status = 2
            # return "体重过重"
        return self.current_status

    def validate_weight_increase_proper(self):
        today = datetime.datetime.now()
        pre_preg_date = datetime.datetime.strptime(self.pre_production_time, '%Y-%m-%d')
        self.preg_weeks = 1 + (280 - (pre_preg_date - today).days) // 7
        print(self.preg_weeks)
        self.incr_percent()
        result = self.cal_pred_weight()
        return result

    def update_user(self, key, value):
        setattr(self, key, value)

    def reset(self):
        # print('disease_flag', self.disease_flag)
        self.disease_flag = not self.disease_flag
        # print('disease_flag', self.disease_flag)


mock_user = MockUser()

if __name__ == "__main__":
    print(mock_user.BMI())
    mock_user.update_user('weight_now', '70')
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag)
    print('---------')

    mock_user.reset()
    print(mock_user.update_user('weight_now', '50'))
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag)
    print('---------')

    mock_user.reset()
    mock_user.update_user('weight_now', '66')
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag)
    print('---------')

