# -*- coding: UTF-8 -*-
from common.wulai_constant import DEFAULT_ERROR_CODE, DEFAULT_ERROR_MSG


class WulaiException(Exception):
    def __init__(self, code=DEFAULT_ERROR_CODE, msg=DEFAULT_ERROR_MSG):
        self.code = code
        self.msg = msg

    def get_error_code(self):
        return self.code

    def get_error_msg(self):
        return self.msg

    def __str__(self):
        return u"code:{}, msg:{}".format(self.code, self.msg)


class InvalidHeadersException(WulaiException):
    def __init__(self, code=10001, msg=u'headers invalid'):
        super(InvalidHeadersException, self).__init__(code, msg)


class InvalidSignException(WulaiException):
    def __init__(self, code=10002, msg=u'sign invalid'):
        super(InvalidSignException, self).__init__(code, msg)


class InvalidParamsException(WulaiException):
    def __init__(self, code=10003, msg=u'params invalid'):
        super(InvalidParamsException, self).__init__(code, msg)


class ThirdPartyException(WulaiException):
    def __init__(self, code=20000, msg=u'service error'):
        super(ThirdPartyException, self).__init__(code, msg)
