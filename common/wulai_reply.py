# -*- coding: UTF-8 -*-
from common import wulai_logger as logger
import json
from functools import partial
from common import utils
# from common import
from conf import constant
# from utils import CONFIG, _redis_inst
import string
from .mock_user import mock_user
import random


def get_reply(params):
    """
       处理 webhook 候选回复， 返回给吾来平台
       :param params: 吾来平台消息路由传入参数
       """
    logger.info("\r\n")
    logger.info("get_reply start".center(80, '-'))
    print('params',params)
    # 获取基本信息
    query = params["msg_body"]['text']['content']
    print("----------------------------------------------")
    print(query)
    c_flag = 0

    for one_response in params["bot_response"]["suggested_response"]:
        try:
            source = one_response["source"]
        except Exception:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":

            # 获取实体列表引用和action的值

            task = one_response['detail']['task']
            entities = task['entities']
            action = task['action']
            logger.info('动作:\t%s' % action)

            if query.strip() in {"退出", "清空", "结束", 'quit', 'exit'}:
                # 提前结束任务
                entities = utils.kill_task(entities)
                task['state'] = 1
                task['entities'] = entities
                text = '感谢使用， 再见！'
                utils.set_next_reply(one_response, text)
                continue

                # 根据对话单元别名进行逻辑处理
            if action == '':
                pass

            # if action == "guide_health_consult":
            #     print("guide_health_consultguide_health_consultguide_health_consultguide_health_consultguide_health_consult")
            #     print('query', query)
            #     if query == "菲仕兰":
            #         health_steps = utils.get_entity(entities, 'fsl_guide_health_consultation')
            #         utils.update_entity(one_response, health_steps, constant.TP_FLAG_Y)

            if action == "guide_health_consult":
                print("guide_health_consultguide_health_consultguide_health_consultguide_health_consultguide_health_consult")
                print('query', query)
                if query == "健康咨询":
                    health_steps = utils.get_entity(entities, 'fsl_guide_health_consultation')
                    utils.update_entity(one_response, health_steps, constant.TP_FLAG_Y)

            elif action == "ask_current_weight":
                print("ask_current_weightask_current_weightask_current_weightask_current_weightask_current_weightask_current_wei")
                # current_weight = utils.get_entity(entities, 'fsl_num')
                # print('current_weight', current_weight)
                current_weight_value = query[0:-3]
                print(current_weight_value)
                mock_user.current_status = mock_user.update_user('weight_now', current_weight_value)
                health_steps = utils.get_entity(entities, 'fsl_current_weight_num')
                utils.update_entity(one_response, health_steps, constant.TP_FLAG_Y)
                # task['state'] = 1
                # task['entities'] = entities
                # one_response["response"] = [
                #     {
                #         "delay_ts": 0,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "image": {
                #                 "resource_url": "http://139.219.128.71:8161/pic/bmi_big.jpg",
                #                 "thumb_url": "http://139.219.128.71:8161/pic/bmi.jpg",
                #             }
                #         },
                #         "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                #         "suggested_response": []
                #     },
                #     {
                #         "delay_ts": 0,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "text": {
                #                 "content": "是否继续向我咨询?\n",
                #                 "tts": None,
                #                 "tts_type": "AMR"
                #             }
                #         },
                #         "show_content": "为了更好的给您提供到建议，请您点击以下链接填写<plugin-page src='/pages/openpage/openpage'>孕期饮食问卷（FFQ)</plugin-page>。\n",
                #         "suggested_response": []
                #     }
                #
                # ]


            # elif action == "fsl_pregnancy_disease":
            #     flag = 1
            #
            #     #有病体重正常
            #     if flag == 1:
            #         one_response["response"] = [
            #             {
            #                 "delay_ts": 0,
            #                 "handle_routine": None,
            #                 "msg_body": {
            #                     "extra": "",
            #                     "text": {
            #                         "content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
            #                         "tts": None,
            #                         "tts_type": "AMR"
            #                     }
            #                 },
            #                 "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
            #                 "suggested_response": []
            #             },
            #             {
            #                 "delay_ts": 1,
            #                 "handle_routine": None,
            #                 "msg_body": {
            #                     "extra": "",
            #                     "text": {
            #                         "content": "是否继续向我咨询?\n",
            #                         "tts": None,
            #                         "tts_type": "AMR"
            #                     }
            #                 },
            #                 "show_content": "是否继续向我咨询?\n",
            #                 "suggested_response": []
            #             }
            #         ]
            #
            #     # 有病体重不正常
            #     if flag == 2:
            #         one_response["response"] = [
            #             {
            #                 "delay_ts": 0,
            #                 "handle_routine": None,
            #                 "msg_body": {
            #                     "extra": "",
            #                     "text": {
            #                         "content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
            #                         "tts": None,
            #                         "tts_type": "AMR"
            #                     }
            #                 },
            #                 "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
            #                 "suggested_response": []
            #             },
            #             {
            #                 "delay_ts": 1,
            #                 "handle_routine": None,
            #                 "msg_body": {
            #                     "extra": "",
            #                     "text": {
            #                         "content": "是否继续向我咨询?\n",
            #                         "tts": None,
            #                         "tts_type": "AMR"
            #                     }
            #                 },
            #                 "show_content": "是否继续向我咨询?\n",
            #                 "suggested_response": []
            #             }
            #         ]

                    # utils.set_next_reply(one_response, msg)


            # elif action == "weight_increase_status":
            #     disease_steps = utils.get_entity(entities, 'fsl_confirm')
            #     print('user_steps', disease_steps)
            #     if disease_steps['value']  == "yes":
            #         daily_steps = utils.get_entity(entities, 'fsl_weight_normal_flag')
            #         utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
            #         msg = '给出个人建议\n'
            #         # for i in range(len(entities)):
            #         #     if entities[i]['name'] in ["last_entity_name"]:
            #         #         print("cCCCCCCCCCCCCCCCCCCCC")
            #         #         entities[i]['seg_value'] = 'fsl_thank_for_advice'
            #         #         task['action'] = 'fsl_thank_for_advice'
            #         #         entities[i]['value'] = 'fsl_weight_normal_flag'
            #         # utils.set_next_reply(one_response, msg)
            #     elif disease_steps['value'] == "no":
            #         daily_steps = utils.get_entity(entities, 'fsl_weight_normal_flag')
            #         utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)


            # elif action == "fsl_continue_consult_doctor":
            #     consult_steps = utils.get_entity(entities, 'fsl_confirm')
            #     print('user_steps', consult_steps)
            #     if isinstance(consult_steps,dict):
            #         if consult_steps['value']  == "yes":
            #             daily_steps = utils.get_entity(entities, 'fsl_continue_suggest_flag')
            #             utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
            #         elif disease_steps['value'] == "no":
            #             daily_steps = utils.get_entity(entities, 'fsl_weight_normal_flag')
            #             utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)

            elif action == "ask_daily_steps":
                print("ask_daily_stepsask_daily_stepsask_daily_stepsask_daily_stepsask_daily_stepsask_daily_stepsask_daily_stepsask_daily_steps")

                current_weight_value = query[0:-2]
                print(current_weight_value)
                print(type(current_weight_value))
                if query.isdigit():
                    mock_user.current_status = mock_user.update_user('weight_now', query)
                    if int(query) > 65 and query is not None:
                        mock_user.current_status = 2
                    elif int(query) < 45 and query is not None:
                        mock_user.current_status = 1
                    else:
                        mock_user.current_status = 0
                else:
                    mock_user.current_status = mock_user.update_user('weight_now', current_weight_value)
                    if int(current_weight_value) > 65 and current_weight_value is not None:
                        mock_user.current_status = 2
                    elif int(current_weight_value) < 45 and current_weight_value is not None:
                        mock_user.current_status = 1
                    else:
                        mock_user.current_status = 0



                user_steps = utils.get_entity(entities, 'fsl_num')
                print('user_steps', user_steps)
                print("dsaafdsafsadfsadfdsafsadfsadfsadfsad")
                daily_steps = utils.get_entity(entities, 'fsl_daily_step_count')
                utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
                # task['state'] = 1
                # task['entities'] = entities

                # one_response["response"] = [
                #     {
                #         "delay_ts": 0,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "image": {
                #                 "resource_url": "http://139.219.128.71:8161/pic/bmi_big.jpg",
                #                 "thumb_url": "http://139.219.128.71:8161/pic/bmi.jpg",
                #             }
                #         },
                #         "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                #     },
                #     {
                #         "delay_ts": 1,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "text": {
                #                 "content": "为了更好的给您提供到建议，请您点击以下链接填写。\n",
                #                 "tts": None,
                #                 "tts_type": "AMR"
                #             }
                #         },
                #         "show_content": "为了更好的给您提供到建议，请您点击以下链接填写。\n",
                #     },
                #     {
                #         "delay_ts": 2,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "text": {
                #                 "content": "<plugin-page src='/pages/openpage/openpage'>孕期饮食问卷（FFQ)</plugin-page>。\n",
                #                 "tts": None,
                #                 "tts_type": "AMR"
                #             }
                #         },
                #         "show_content": "<plugin-page src='/pages/openpage/openpage'>孕期饮食问卷（FFQ)</plugin-page>。\n",
                #     }
                #
                # ]


                # one_response["response"] = [
                #     {
                #         "delay_ts": 0,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "text": {
                #                 "content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                #                 "tts": None,
                #                 "tts_type": "AMR"
                #             }
                #         },
                #         "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                #         "suggested_response": []
                #     },
                #     {
                #         "delay_ts": 1,
                #         "handle_routine": None,
                #         "msg_body": {
                #             "extra": "",
                #             "text": {
                #                 "content": "是否继续向我咨询? 请填写继续或不继续\n",
                #                 "tts": None,
                #                 "tts_type": "AMR"
                #             }
                #         },
                #         "show_content": "是否继续向我咨询? 请填写继续或不继续\n",
                #         "suggested_response": []
                #     }
                # ]

                    # action = "fls_continue_to_consult_or_not"
                # next_reply = "请问是否有孕期疾病？\n请输入 yes  或  no"
                # utils.update_entity(one_response, next_reply,constant.TP_FLAG_Y)

            if action == 'fsl_thanks_and_continue':

                print("dsadasfasdfsdafsdafasdf")
                if query == "继续":
                    daily_steps = utils.get_entity(entities, 'fsl_thanks_and_continue_flag')
                    utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    print("dsdasfsadfasdfsadfsadfasdfsadfsdafdsa ", mock_user.current_status)
                    # mock_user.current_status = 2
                    # mock_user.current_status = random.randint(1, 4)
                    print(mock_user.current_status)
                    print(type(mock_user.current_status))
                    insert_value ={
                        "delay_ts": -1,
                        "handle_routine": None,
                        "msg_body": {
                            "extra": "",
                            "text": {
                                "content": "感谢您对我的信任，目前我的建议仅适用于没有孕期相关疾病的妈妈，之后的建议仅供参考，但任然强烈建议您遵从医嘱\n",
                                "tts": None,
                                "tts_type": "AMR"
                            }
                        },
                        "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您。\n",
                        "suggested_response": []
                    }
                    if mock_user.current_status >= 2:
                        # constant.too_heavy.insert(-1,insert_value)
                        one_response["response"] = constant.too_heavy_1
                    elif mock_user.current_status == 1:
                        # constant.too_heavy.insert(-1, insert_value)
                        one_response["response"] = constant.too_light_1
                    elif mock_user.current_status == 0:
                        # constant.too_heavy.insert(-1, insert_value)
                        one_response["response"] = constant.normal_weight_1

                elif query == "不继续":
                    print("sdaafsadfsdafsadfsdafasfd")
                    daily_steps = utils.get_entity(entities, 'fsl_thanks_and_continue_flag')
                    utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)
                    entities = utils.kill_task(entities)
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }

                    }
                    one_response["response"] = [
                        {
                            "delay_ts": 0,
                            "handle_routine": None,
                            "msg_body": {
                                "extra": "",
                                "text": {
                                    "content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                                    "tts": None,
                                    "tts_type": "AMR"
                                }
                            },
                            "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                            "suggested_response": []
                        },
                    ]
                    task['state'] = 1
                    task['entities'] = entities

            if action == "fsl_continue_to_consult_or_not":

                print("fsl_continue_to_consult_or_notfsl_continue_to_consult_or_notfsl_continue_to_consult_or_notfsl_continue_to_consult_or_notfsl_continue_to_consult_or_notfsl_continue_to_consult_or_not")

                disease_steps = utils.get_entity(entities, 'fsl_confirm')
                print('user_steps', disease_steps)

                # if query == "继续":
                #     daily_steps = utils.get_entity(entities, 'fsl_whether_conitinue_consult_flag')
                #     utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
                #     entities = utils.kill_task(entities)
                #     task['state'] = 1
                #     task['entities'] = entities
                #     one_response['handle_routine'] = {
                #         "label_detail": {
                #             "is_label": False
                #         },
                #         "route": True,
                #         "route_detail": {
                #             "key": ""
                #         }
                #
                #     }
                #     if mock_user.current_status == 2:
                #         one_response["response"] = constant.too_heavy
                #     elif mock_user.current_status == 1:
                #         one_response["response"] = constant.too_light
                #     elif mock_user.current_status == 0:
                #         one_response["response"] = constant.normal_weight
                #
                # elif query == "不继续":
                #     daily_steps = utils.get_entity(entities, 'fsl_whether_conitinue_consult_flag')
                #     utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)
                #     entities = utils.kill_task(entities)
                #     one_response['handle_routine'] = {
                #         "label_detail": {
                #             "is_label": False
                #         },
                #         "route": True,
                #         "route_detail": {
                #             "key": ""
                #         }
                #
                #     }
                #     one_response["response"] = [
                #         {
                #             "delay_ts": 0,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                #             "suggested_response": []
                #         },
                #     ]
                #     task['state'] = 1
                #     task['entities'] = entities

                # if query == "7000":
                # daily_steps = utils.get_entity(entities, 'fsl_whether_conitinue_consult_flag')
                # utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)

                # user_steps = utils.get_entity(entities, 'fsl_num')
                # print('user_steps', user_steps)
                #
                # daily_steps = utils.get_entity(entities, 'fsl_daily_step_count')
                # utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)

                print("dsaasfsadfsdafsadfsadfsadfsdafsadfsdafsad")
                # fsl_num_steps = utils.get_entity(entities, 'fsl_num')
                # print('fsl_num_steps', fsl_num_steps)

                daily_steps = utils.get_entity(entities, 'fsl_whether_conitinue_consult_flag')
                utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)

                # if c_flag

                # task['state'] = 1
                # task['entities'] = entities
                # mock_user.disease_flag = True
                print("iiiiiiiii ",mock_user.disease_flag)
                # mock_user.disease_flag = random.randint(0, 1)
                if mock_user.disease_flag:
                    print("大撒菲萨地方撒地方撒地方撒地方是大")
                    # for i in range(len(entities)):
                    #     if entities[i]['name'] in ["last_entity_name"]:
                    #         print("cCCCCCCCCCCCCCCCCCCCC")
                    #         entities[i]['seg_value'] = ''
                    #         task['action'] = 'fsl_thanks_and_continue'
                    #         entities[i]['value'] = ''
                    one_response["response"] = [
                        {
                            "delay_ts": 0,
                            "handle_routine": None,
                            "msg_body": {
                                "extra": "",
                                "image": {
                                    "resource_url": "http://139.219.128.71:8161/pic/bmi_big.jpg",
                                    "thumb_url": "http://139.219.128.71:8161/pic/bmi.jpg",
                                }
                            },
                            "show_content": "",
                            "suggested_response": []
                        },
                        {
                            "delay_ts": 1,
                            "handle_routine": None,
                            "msg_body": {
                                "extra": "",
                                "text": {
                                    "content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                                    "tts": None,
                                    "tts_type": "AMR"
                                }
                            },
                            "show_content": "根据您预留的信息，我们发现您有过妊娠糖尿病史（或妊娠高血压病史），请预约专业的妇产科医生进行咨询。\n",
                            "suggested_response": []
                        },
                        {
                            "delay_ts": 2,
                            "handle_routine": None,
                            "msg_body": {
                                "extra": "",
                                "text": {
                                    "content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                                    "tts": None,
                                    "tts_type": "AMR"
                                }
                            },
                            "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您\n",
                            "suggested_response": []
                        },
                        {
                            "delay_ts": 3,
                            "handle_routine": None,
                            "msg_body": {
                                "extra": "",
                                "text": {
                                    "content": "是否继续向我咨询？请填写继续或不继续\n",
                                    "tts": None,
                                    "tts_type": "AMR"
                                }
                            },
                            "show_content": "􏰋􏱺􏱻􏱼􏱽􏰊􏰳􏰴􏰋􏱺􏱻􏱼􏱽􏰊􏰳􏰴是否继续向我咨询?请填写继续或不继续\n",
                            "suggested_response": []
                        }
                    ]
                    # mock_user.reset()
                    # task['state'] = 1
                    # task['entities'] = entities
                else:
                    print("dsdasfsadfasdfsadfsadfasdfsadf1111sdafdsa ", mock_user.current_status)
                    # mock_user.current_status = 2
                    # mock_user.current_status = random.randint(1, 4)
                    print(mock_user.current_status)
                    print(type(mock_user.current_status))
                    if mock_user.current_status >= 2:
                        one_response["response"] = constant.too_heavy
                    elif mock_user.current_status == 1:
                        one_response["response"] = constant.too_light
                    elif mock_user.current_status == 0:
                        one_response["response"] = constant.normal_weight
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }

                    }

                mock_user.reset()
                # daily_steps = utils.get_entity(entities, 'fsl_whether_conitinue_consult_flag')
                # utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)



            # elif action == "fls_continue_choice":
            #     print("AAAA")
            #     daily_steps = utils.get_entity(entities, 'fsl_continue_or_not')
            #     utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
            #     consult_steps = utils.get_entity(entities, 'fsl_confirm')
            #     print('user_steps', consult_steps)
            #     if isinstance(consult_steps, dict):
            #         if consult_steps['value'] == "yes":
            #             daily_steps = utils.get_entity(entities, 'fsl_continue_suggest_flag')
            #             utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
            #         elif disease_steps['value'] == "no":
            #             daily_steps = utils.get_entity(entities, 'fsl_weight_normal_flag')
            #             utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)
                # elif flag == 2:

                # consult_steps = utils.get_entity(entities, 'fsl_confirm')
                # print('user_steps', consult_steps)
                # if isinstance(consult_steps, dict):
                #     if consult_steps['value'] == "yes":
                #         daily_steps = utils.get_entity(entities, 'fsl_continue_suggest_flag')
                #         utils.update_entity(one_response, daily_steps, constant.TP_FLAG_Y)
                #     elif disease_steps['value'] == "no":
                #         daily_steps = utils.get_entity(entities, 'fsl_weight_normal_flag')
                #         utils.update_entity(one_response, daily_steps, constant.TP_FLAG_N)
                #
                # flag = 2
                # if flag == 1:
                #     # 体重正常无疾病
                #     for i in range(len(entities)):
                #         if entities[i]['name'] in ["last_entity_name"]:
                #             print("cCCCCCCCCCCCCCCCCCCCC")
                #             entities[i]['seg_value'] = ''
                #             task['action'] = ''
                #             entities[i]['value'] = ''
                #
                #     one_response['handle_routine'] = {
                #         "label_detail": {
                #             "is_label": False
                #         },
                #         "route": True,
                #         "route_detail": {
                #             "key": ""
                #         }
                #
                #     }
                #     one_response['detail']['task']['action'] = ""
                #     one_response["response"] = [
                #         {
                #             "delay_ts": 0,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "非常高兴看到在目前这个阶段，您的体重增长处在正常范围内，要继续保持喔。",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "非常高兴看到在目前这个阶段，您的体重增长处在正常范围内，要继续保持喔。",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 1,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "[个人健康建议]\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "[个人健康建议]\n",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 2,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "[膳食搭配建议]\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "[膳食搭配建议]\n",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 3,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您。\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您。\n",
                #             "suggested_response": []
                #         }
                #     ]
                # elif flag == 2:
                #     #  体重不正常无疾病
                #     print("你好")
                #     # utils.set_next_reply(one_response, msg)
                #     for i in range(len(entities)):
                #         if entities[i]['name'] in ["last_entity_name"]:
                #             print("cCCCCCCCCCCCCCCCCCCCC")
                #             entities[i]['seg_value'] = ''
                #             task['action'] = ''
                #             entities[i]['value'] = ''
                #     one_response['handle_routine'] = {
                #         "label_detail": {
                #             "is_label": False
                #         },
                #         "route": True,
                #         "route_detail": {
                #             "key": ""
                #         }
                #
                #     }
                #     one_response["response"] = [
                #         {
                #             "delay_ts": 0,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "您的体重增长目前已超过正常值，您要注意饮食调整和适量运动喔。（您的体重增长目前并未达标，您要注意营养摄入，蛋白质和热量恰恰是胎儿生长发育的重要因素喔。）",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "您的体重增长目前已超过正常值，您要注意饮食调整和适量运动喔。（您的体重增长目前并未达标，您要注意营养摄入，蛋白质和热量恰恰是胎儿生长发育的重要因素喔。）",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 1,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "为了更好的给您提供到建议，请您点击以下链接填写孕期饮食问卷（FFQ）。\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "为了更好的给您提供到建议，请您点击以下链接填写孕期饮食问卷（FFQ）。\n",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 2,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "[个人健康建议]\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "[个人健康建议]\n",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 3,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "[膳食搭配建议]\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "[膳食搭配建议]\n",
                #             "suggested_response": []
                #         },
                #         {
                #             "delay_ts": 4,
                #             "handle_routine": None,
                #             "msg_body": {
                #                 "extra": "",
                #                 "text": {
                #                     "content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您。\n",
                #                     "tts": None,
                #                     "tts_type": "AMR"
                #                 }
                #             },
                #             "show_content": "如您还有其他孕期问题想了解，可以随时问我，希望我的回答能帮到您。\n",
                #             "suggested_response": []
                #         }
                #     ]

            # elif action == "ask_daily_steps":
            #     user_steps = utils.get_entity(entities, 'fsl_num')
            #     print('user_steps', user_steps)
            #     daily_steps = utils.get_entity(entities, 'fsl_daily_step_count')
            #     utils.update_entity(one_response, daily_steps, constant.TP_FLAG)
            #     next_reply = "请问是否有孕期疾病？\n请输入 yes  或  no"
            #     utils.update_entity(one_response, next_reply,constant.TP_FLAG)



        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")



    logger.info('[get_reply]:\t文本处理结束')
    return params["bot_response"]["suggested_response"]
