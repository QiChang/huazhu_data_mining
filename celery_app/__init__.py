# -*- coding: utf-8 -*-

from celery import Celery,platforms

platforms.C_FORCE_ROOT = True
from celery_app.celery_config import \
    (
    CELERY_BROKER_URL,
    CELERY_RESULT_BACKEND,
    PROJECT_NAME,
    CELERY_IMPORTS,
    CELERY_QUEUES,
    CELERY_DEFAULT_QUEUE,
    CELERY_ROUTES,
    CELERY_ACCEPT_CONTENT
    )

# 创建celery实例
celery = Celery(PROJECT_NAME,
    broker=CELERY_BROKER_URL,
    backend=CELERY_RESULT_BACKEND)

# 通过 Celery 实例加载配置模块
celery.config_from_object('celery_app.celery_config')
celery.conf['CELERY_IMPORTS'] = CELERY_IMPORTS
celery.conf['CELERY_DEFAULT_QUEUE'] = CELERY_DEFAULT_QUEUE
celery.conf['CELERY_QUEUES'] = CELERY_QUEUES
celery.conf['CELERY_ROUTES'] = CELERY_ROUTES
celery.conf['CELERY_ACCEPT_CONTENT']=CELERY_ACCEPT_CONTENT



