# -*- coding: utf-8 -*-
"""
  @Company: Laiye Ltd.
  @License: Copyright © 2018 Laiye Ltd. All rights reserved.
  @Author: QiChang Yin
  @Date: 2018-11-01
  @Instruction: the config file of task worker
"""

from celery_app import celery
# from rongcloud_helper import handle_wulai_async_msg
import datetime
import utils
import os
import pandas as pd
from pandas.core.frame import DataFrame
from common import cluster

REDIS_EXPIRES = 1 * 24 * 60 * 60

@celery.task
def callback():
    # handle_wulai_async_msg(data)
    # callback.callback()
    redis_cli = utils.get_redis_cli()
    # redis_cli.set("cluster_status", "begin", ex=REDIS_EXPIRES)
    cluster_dir = os.path.join(os.path.dirname(__file__), '../static/cluster_txt')
    IDF = pd.read_table(cluster_dir + "/chinese-idf-wordlist.txt", names=['word', 'number'],
                        sep='\t').set_index('word').T.to_dict('records')[0]

    SYNONYM_LIST = pd.read_table(cluster_dir + "/syn_word_dup.txt",
                                 names=['word', 'word_syn'], error_bad_lines=False).set_index('word').T.to_dict(
        'records')[0]
    print("============================================================")
    upload_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
    # dir_name = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload")
    # cluster_dataset = os.path.join(os.path.dirname(__file__), '../static/upload')  # 文件的暂存路径
    cluster_dataset_path = upload_path + "/input.xlsx" if redis_cli.get("file_type") else upload_path + "/input.csv"
    # cluster_dataset_path = upload_path + "/input.xls" if isNotCvs else upload_path+"/input.csv"
    print(upload_path, cluster_dataset_path)
    try:
        # if type == str(0):
        # dic = cluster.question_cluster(cluster_dataset_path, IDF, SYNONYM_LIST)
        # dic.to_excel(upload_path + '/output.xls', sheet_name='output', index=False)
        starttime = datetime.datetime.now()
        length_sent, length_set, length_cluster, h3, length_ceshi_count, dic, repre = cluster.question_cluster(
            cluster_dataset_path, IDF, SYNONYM_LIST)

        data = DataFrame(repre)
        dic.to_excel(upload_path + "/data1222.xls", index=False)
        data.to_excel(upload_path + "/reper111.xls", index=False)
        endtime = datetime.datetime.now()
        length_time = (endtime - starttime).seconds
        count_dic = {}
        count_dic['最小类元素的个数'] = 5
        count_dic['聚类时间'] = length_time
        count_dic['总共数据量'] = length_sent
        count_dic['去重后条数'] = length_set
        count_dic['共聚出条数'] = length_cluster
        count_dic['有意义的类数'] = length_cluster - 1
        count_dic['有意义的类中问题频率和'] = h3
        count_dic['有意义的类中问题去重后'] = length_ceshi_count
        count_dic['无意义类数'] = 1
        count_df = pd.DataFrame([count_dic])
        count_df = count_df.T
        count_df.to_excel(upload_path + "/count.xls", header=False)

        # else:
        #     get_result(cluster_dataset_path, upload_path + '/output.xls')
        redis_cli.set("cluster_status", "finish", ex=REDIS_EXPIRES)
    except Exception as e:
        print(e)