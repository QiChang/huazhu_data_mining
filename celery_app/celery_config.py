#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
  @Company: Laiye Ltd.
  @License: Copyright © 2018 Laiye Ltd. All rights reserved.
  @Author: QiChang Yin
  @Date: 2018-11-01
  @Instruction: the config file of celery and redis
"""

from os.path import dirname, abspath
from kombu import Queue, Exchange

# Celery介绍：
# USE_REMOTE_REDIS_FLAG = True 表示采用的远程的redis作为消息中间件和结果存储，等于False时，则表示采用本地redis作为消息
# 消息中间件 Broker
# Broker，即为任务调度队列，接收任务生产者发来的消息（即任务），将任务存入队列。Celery 本身不提供队列服务，官方推荐使用 RabbitMQ 和 Redis 等。
# Worker 是执行任务的处理单元，它实时监控消息队列，获取队列中调度的任务，并执行它。
# 任务结果存储 Backend
# Backend 用于存储任务的执行结果，以供查询。同消息中间件一样，存储也可使用 RabbitMQ, redis 和 MongoDB 等。
# 使用celery包含三个方面：1. 定义任务函数。2. 运行celery服务。3. 客户应用程序的调用。


# 获取工程目录
PROJECT_PATH = dirname(dirname(abspath(__file__)))


# debug调试还是线上，线上运行为false，本地测试是true
DEBUG = False
# 是否使用celery异步调度队列来处理
USE_CELERY_FLAG = True
# 使用远程redis还是本地redis作为缓存，
USE_REMOTE_REDIS_FLAG = True


# 项目名字
PROJECT_NAME = 'wx-mini-backend'


# 中间件和结果存储
if USE_REMOTE_REDIS_FLAG:
    REDIS_HOST = 'pocdemo.redis.cache.chinacloudapi.cn'
    REDIS_PORT = 6379
    REDIS_PWD = 'XYHROYZ3Qlymw9MeM3g6Ggi4NPpJbh64+Y8i4WKOvms='
    BROKER_REDIS_DB = 1
    BACKEND_REDIS_DB = 0
    CELERY_BROKER_URL = 'redis://:{REDIS_PWD}@{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}'.format(
                                                                   REDIS_HOST=REDIS_HOST,
                                                                   REDIS_PORT=REDIS_PORT,
                                                                   REDIS_PWD=REDIS_PWD,
                                                                   REDIS_DB=BROKER_REDIS_DB)
    CELERY_RESULT_BACKEND = 'redis://:{REDIS_PWD}@{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}'.format(
                                                                   REDIS_HOST=REDIS_HOST,
                                                                   REDIS_PORT=REDIS_PORT,
                                                                   REDIS_PWD=REDIS_PWD,
                                                                   REDIS_DB=BACKEND_REDIS_DB)
else:
    CELERY_BROKER_URL = 'redis://localhost:6379/1'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'


# 指定时区，默认是 UTC
CELERY_TIMEZONE='Asia/Shanghai'
# 指定导入的任务模块
CELERY_IMPORTS = ('celery_app.callback',
                  'celery_app.rongcloud')


CELERY_TASK_RESULT_EXPIRES = 7200  # celery任务执行结果的超时时间，
CELERYD_CONCURRENCY = 1 if DEBUG else 25 # celery worker的并发数 也是命令行-c指定的数目 根据服务器配置实际更改 一般25即可
CELERYD_MAX_TASKS_PER_CHILD = 500  # 每个worker执行了多少任务就会死掉，我建议数量可以大一些，比如200
CELERYD_PREFETCH_MULTIPLIER = 4 # celery worker 每次去rabbitmq取任务的数量，我这里预取了4个慢慢执行,因为任务有长有短没有预取太多


# 默认队列
CELERY_DEFAULT_QUEUE = 'default_queue'

CELERY_ACCEPT_CONTENT = ['json', 'pickle']


# 定义任务队列
CELERY_QUEUES = (
    Queue(
        name='default_queue',
        exchange=Exchange('default_exchange', 'direct'),
        routing_key='default_key'),
    Queue(
        name='callback_queue',
        exchange=Exchange('callback_exchange', 'direct'),
        routing_key='callback_key'),
    Queue(
        name='rongcloud_queue',
        exchange=Exchange('rongcloud_exchange', 'direct'),
        routing_key='rongcloud_key'),
)


# 定义路由方式
CELERY_ROUTES = {
    'celery_app.callback.callback':
        {
            'queue': 'callback_queue',
            'routing_key': 'callback_key'
        },
    'celery_app.rongcloud.rongcloud':
        {
            'queue': 'rongcloud_queue',
            'routing_key': 'rongcloud_key'
        },
}


