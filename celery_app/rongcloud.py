# -*- coding: utf-8 -*-

from celery_app import celery
# import rongcloud_helper as rongcloud

@celery.task
def rongcloud(res,user_id):
    if res.get('bot_response'):
        rongcloud.handle_wulai_sync_msg(user_id, res['bot_response']['suggested_response'])


# @celery.task
# def send_wulai_sug(res,user_id):
#     rongcloud_helper.handle_wulai_async_msg({
#         "user_id": user_id,
#         "msg_body": {
#             "sug": res
#         }
#     })