# 接口文档

服务器地址： https://mplugin.wul.ai

## 旧有接口
### 登录
**请求路径**： /login
**请求参数**：
```
{
    "user_id": "test001", //用户ID, 必填
    "pubkey": "xxx", //平台pubkey,必填
    "ts": "xxx", //时间戳,必填
    "sign": "xxx", //签名,必填
    "nickname": "test001", //昵称,可选
    "avatar": "", //头像,可选
    "env": "dev", //环境，dev或prod,可选
}
```

**签名方法**

sign的值为ts+user_id+TOKEN的哈希值，
其中TOKEN为dLEMQa9JxmCINoarLpJUe56ndbBXcHmJNGATbNaIRhnEFuOP

**返回**：

```
{
	"res": 1,
    "user_id": user_id
}
```

### websocket接口
**请求路径**: /chat

**对话请求参数**：


```
{
    "data": // data为下面字段的stringify
    "
    {
      "sender": "xxx", //用户ID,必填
      "pubkey": "xxx", //平台pubkey,必填
      "env": "dev", //环境，dev或prod,可选
      "async": true, //是否异步方式,可选
      "msg_content": {
      	"msg_type": 0, // 0标示文本
        "content": "XXX",// 用户发送内容
      }
    }
    "

}
```

**返回**：

```
{
    "code": 0,
    "data": {'msg_body':
                 {
                     'text':
                      {
                          'content': 'XXX',
                           'tts_type': 'AMR',
                           'tts': None
                      },
                     'extra': ''
                 },
             'delay_ts': 0,
             'show_content': 'XXX',
             'handle_routine': None
             }
}
```

**心跳请求参数**：


```
{
    msg_content: {
      content: '[heartbeat]'
    }
}
```

**返回**：

```
{
    'code': 0,
    'msg': "Success"
}
```


## 融云相关接口
### 获取token
**请求路径**： /token/rongcloud
**请求参数**：
```
{
    "openid": "test001", //用户ID, 必填
    "pubkey": "xxx", //平台pubkey,必填
    "nickname": "test001", //昵称,可选
    "avatar": "", //头像,可选
    "env": "dev", //环境，dev或prod,可选
}
```

**返回**：

```
{
	code: 200,
    userId: "flingjie_test001",
    token: "1iG39sNnw9i6FqzHAioTLZCpjj/zWNrkQTmMrS995d1WK6K8uQ…yl6h0knRBLxLrfSamGNDCrILFHQNGcGJ+5XNd/4141wcTT7I="
}
```
## 其他接口

### 获取客户配置信息
**请求路径**: /get_configure?client=XXX

**请求参数**：


```
/get_configure?client=XXX
```

**返回**：

```
用户配置的Json格式
```
### 语音合成

**请求路径**：/tts

**请求参数**：


```
{
    "content": "test001", //文本内容,必填
    "content_type": "text", //格式,必填
}
```

**返回**：

```
语音文件地址
```

### 上传图片

**请求路径**：/upload

**请求参数**：


```
将文件放到字段为images的表单中上传
```

**返回**：

```
{
    "result": "处理结果",
    "image_url": "xxx",
    "thumbnail_url": "xxx"
}
```

### 语音识别
**请求路径**: /asr

**请求参数**：


```
{
    "speech_id": "speech001", //语音标识ID, 必填
    "content": "xxx", //语音base64编码,必填
    "is_end": 1, //是否最后,0或1,必填
    "content_len": 1000, //语音长度,必填
    "seq": 0, //之前的长度,必填
}
```

**返回**：

```
{
	"speech_text": 识别后的结果,
    "url": 语音地址
}
```

### 传递user_uuid 获取 openid
**请求路径**: /oauth/openid

**POST 请求参数**：


```
{
    "user_uuid": "xxx", //服务器保存用户态的token, 必填
}
```

**返回**：

```
{
	"openid": redis中存储的openid,
}
```

### 获取 openid 并存储
**请求路径**: /oauth/login

**POST 请求参数**：


```
{
    "js_code": "xxx", //wx_login()获取到的code, 必填
    "mini_app_id": "", // 小程序appid, 可选
}
```

**返回**：

```
{
	"user_uuid": 服务器保存用户态的token
	"openid": 微信返回的用户openid,
}
```


### 获取 消息历史记录
**请求路径**: /msg/history

**POST 请求参数**：


```
{
    "user_id": "", //用户ID, 必填
    "pubkey": "", //平台pubkey,必填
    "env": "prod", //环境，prod,可选, 缺省为 prod
    "msg_id": "", //平台消息id, 可选
    "direction": "BACKWARD", // 默认为BACKWARD 表示向后,  FORWARD 表示向前 可选
    "num": 20               //获取历史消息的数量,  可选
}
```

**返回**：

```
{
    "items": [
        {
            "body": {
                "text": {
                    "content": "开会",
                    "tts_type": "AMR",
                    "tts": null
                },
                "extra": ""
            },
            "msg_ts": "1533091277156",
            "msg_id": "474043917135216659",
            "staff_info": null,
            "user_info": {
                "nickname": "mbjy_mockao9jqg1532933231007",
                "avatar_url": ""
            },
            "direction": "USER_SEND",
            "msg_type": "TEXT",
            "extra": ""
        },
        {
            "body": {
                "text": {
                    "content": " 没问题。你想预定WeWork的哪个办公地点的会议室？如：徐家汇、南京西路...",
                    "tts_type": "AMR",
                    "tts": null
                },
                "extra": ""
            },
            "msg_ts": "1533091277501",
            "msg_id": "474043918590640147",
            "staff_info": {
                "real_name": "AI",
                "avatar_url": "http://img.ps.laiye.com/cFaP7pdBo5rwDi0h1gRu2WwTGMnmKNbX.png",
                "nickname": ""
            },
            "user_info": null,
            "direction": "STAFF_SEND",
            "msg_type": "TEXT",
            "extra": ""
        },

        // 图片历史记录
        {
            "body": {
                "image": {
                    "resource_url": "https://laiye-im-saas.oss-cn-beijing.aliyuncs.com/rc-upload-1531967198816-3-IMG_3200.jpg",
                    "thumb_url": ""
                },
                "extra": "",
                "thumb_url": "https://shanghaiossserver.blob.core.chinacloudapi.cn/xiaochengxu/rc-upload-1531967198816-3-IMG_3200.jpg",
                "thumb_w": 160,
                "thumb_h": 152
            },
            "msg_ts": "1533191098295",
            "msg_id": "474462597347180563",
            "staff_info": {
                "real_name": "AI",
                "avatar_url": "http://img.ps.laiye.com/cFaP7pdBo5rwDi0h1gRu2WwTGMnmKNbX.png",
                "nickname": ""
            },
            "user_info": null,
            "direction": "STAFF_SEND",
            "msg_type": "IMAGE",
            "extra": ""
        }        
    ],
    "has_more": true        //是否还有更多的历史消息记录
}

```