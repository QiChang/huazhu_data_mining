import os
import logging

log_level = int(os.getenv('log_level', logging.DEBUG))

debug = bool(int(os.getenv('debug', 1)))

normal_log_path = os.getenv('normal_log_path', 'logs/runing.log')

warn_log_path = os.getenv('warn_log_path', 'logs/warn.log')

error_log_path = os.getenv('error_log_path', 'logs/error.log')

# 邮件接收者， 单人邮件格式为   ’username@host.com‘
# 多人接受者， 邮件格式为   ’username1@host.com, username2@host.com‘
EMAIL_RECEIVER = os.getenv('receiver', 'wangqisheng@laiye.com')

wul_api_host = os.getenv('wul_api_host', 'https://openapi.wul.ai')
# 测试吾来接口地址 101.200.177.95:6124

# 产品试用申请邮件列表
APPLY_NOTIFY = os.getenv('APPLY_NOTIFY', 'wangqisheng@laiye.com')

# REDIS_URL = os.getenv('REDIS_URL', 'redis://localhost:6379')
# REDIS_HOST = os.getenv('REDIS_HOST', 'laiye-shanghai.redis.cache.chinacloudapi.cn')
# REDIS_PORT = os.getenv('REDIS_PORT', 6380)
# REDIS_PWD = os.getenv('REDIS_PWD', 'Jylo7hD48LsoPeU0NZuyD4rdvuaRiJ0HHIFuHKO3ddw=')
# REDIS_DB = os.getenv('REDIS_DB', 0)

REDIS_URL = os.getenv('REDIS_URL', 'redis://127.0.0.1:6379')
REDIS_HOST = "127.0.0.1"
REDIS_PORT = 6379
REDIS_PWD = "123456"
REDIS_DB = 1

multi_process_num = os.getenv('multi_process_num', 0)
autoreload = bool(int(os.getenv('autoreload', 0)))

# 域名检查
host_check_flag = bool(int(os.getenv('host_check_flag', 1)))