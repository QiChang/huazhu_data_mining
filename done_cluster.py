# !/usr/bin/env python
# -*- coding:utf-8 -*-
import time
import os
import pandas as pd
import utils
from common import cluster
REDIS_EXPIRES = 1 * 24 * 60 * 60

def main():
    while True:
        time.sleep(1)
        redis_cli = utils.get_redis_cli()
        if redis_cli.get("cluster_status") == "begin":
            redis_cli.set("cluster_status", "begin", ex=REDIS_EXPIRES)
            cluster_dir = os.path.join(os.path.dirname(__file__), '/static/cluster_txt')
            print(cluster_dir)
            IDF = pd.read_table(cluster_dir + "/chinese-idf-wordlist.txt", names=['word', 'number'],
                                sep='\t').set_index('word').T.to_dict('records')[0]

            SYNONYM_LIST = pd.read_table(cluster_dir + "/syn_word_dup.txt",
                                         names=['word', 'word_syn'], error_bad_lines=False).set_index('word').T.to_dict(
                'records')[0]

            upload_path = os.path.join(os.path.dirname(__file__), "static/upload")
            # cluster_dataset = os.path.join(os.path.dirname(__file__), '../static/upload')  # 文件的暂存路径
            # redis_cli = utils.get_redis_cli()
            cluster_dataset_path = upload_path + "/input.xls" if redis_cli.get("file_type") else upload_path+"/input.csv"
            print(upload_path,cluster_dataset_path)
            try:
                dic = cluster.question_cluster(cluster_dataset_path, IDF, SYNONYM_LIST)
                dic.to_excel(upload_path + '/output.xls', sheet_name='output', index=False)
            except Exception as e:
                print(e)
            redis_cli.set("cluster_status", "finish", ex=REDIS_EXPIRES)
        else:
            print("AAAA")

if __name__ == '__main__':
    main()