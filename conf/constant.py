import pandas as pd
import os

# cluster_dir = os.path.join(os.path.dirname(__file__), '../static/cluster_txt')
cluster_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/cluster_txt")
print(cluster_dir)

IDF = pd.read_table(cluster_dir + "/chinese-idf-wordlist.txt", names=['word', 'number'],
                            sep='\t').set_index('word').T.to_dict('records')[0]

SYNONYM_LIST = pd.read_table(cluster_dir + "/syn_word_dup.txt",
                         names=['word', 'word_syn'], error_bad_lines=False).set_index('word').T.to_dict(
'records')[0]
